package net.dragon9815.playerutils.client.model.item;

import com.google.common.base.*;
import com.google.common.base.Optional;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Maps;
import net.dragon9815.playerutils.common.item.Items;
import net.dragon9815.playerutils.common.reference.Reference;
import net.dragon9815.playerutils.common.util.PlayerData;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.block.model.*;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.client.renderer.vertex.VertexFormat;
import net.minecraft.client.resources.IResourceManager;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import net.minecraftforge.client.model.*;
import net.minecraftforge.common.model.IModelPart;
import net.minecraftforge.common.model.IModelState;
import net.minecraftforge.common.model.TRSRTransformation;
import org.apache.commons.lang3.tuple.Pair;

import javax.annotation.Nullable;
import javax.vecmath.Matrix4f;
import java.util.*;

public class ModelPlayerCard implements IModel, IModelCustomData {
    private static final IModel MODEL = new ModelPlayerCard(false);
    private static ResourceLocation textureUnbound = new ResourceLocation(Reference.MOD_ID, "items/tools/player_card_unbound");
    private static ResourceLocation textureBound = new ResourceLocation(Reference.MOD_ID, "items/tools/player_card_bound");

    private final boolean hasPlayer;

    public ModelPlayerCard(boolean hasPlayer) {
        this.hasPlayer = hasPlayer;
    }

    @Override
    public Collection<ResourceLocation> getDependencies() {
        return ImmutableList.of();
    }

    @Override
    public Collection<ResourceLocation> getTextures() {
        return ImmutableSet.of(textureUnbound, textureBound);
    }

    @Override
    public IBakedModel bake(IModelState state, VertexFormat format, Function<ResourceLocation, TextureAtlasSprite> bakedTextureGetter) {
        ImmutableMap<ItemCameraTransforms.TransformType, TRSRTransformation> transformMap = IPerspectiveAwareModel.MapWrapper.getTransforms(state);

        ImmutableList.Builder<BakedQuad> builder = ImmutableList.builder();

        IBakedModel model = (new ItemLayerModel(ImmutableList.<ResourceLocation>of(hasPlayer ? textureBound : textureUnbound))).bake(state, format, bakedTextureGetter);
        builder.addAll(model.getQuads(null, null, 0));

        return new BakedPlayerCard(this, builder.build(), format, Maps.immutableEnumMap(transformMap));
    }

    @Override
    public IModelState getDefaultState() {
        return TRSRTransformation.identity();
    }

    @Override
    public IModel process(ImmutableMap<String, String> customData) {
        if(customData.containsKey("hasPlayer")) {
            if (customData.get("hasPlayer").equals("true"))
                return new ModelPlayerCard(true);
            else if(customData.get("hasPlayer").equals("false"))
                return new ModelPlayerCard(false);
            else
                throw new IllegalArgumentException("PlayerCard custom data \'hasPlayer\' must have value \'true\' or \'false\' (was \'" + customData.get("hasPlayer") + "\')");
        }

        return new ModelPlayerCard(false);
    }

    private static final class BakedPlayerCard implements IPerspectiveAwareModel {

        private final ModelPlayerCard parent;
        private final ImmutableMap<ItemCameraTransforms.TransformType, TRSRTransformation> transforms;
        private final ImmutableList<BakedQuad> quads;
        private final VertexFormat format;

        public BakedPlayerCard(ModelPlayerCard parent, ImmutableList<BakedQuad> quads, VertexFormat format, ImmutableMap<ItemCameraTransforms.TransformType, TRSRTransformation> transforms) {
            this.parent = parent;
            this.quads = quads;
            this.transforms = transforms;
            this.format = format;
        }

        @Override
        public Pair<? extends IBakedModel, Matrix4f> handlePerspective(ItemCameraTransforms.TransformType cameraTransformType) {
            return IPerspectiveAwareModel.MapWrapper.handlePerspective(this, transforms, cameraTransformType);
        }

        @Override
        public List<BakedQuad> getQuads(@Nullable IBlockState state, @Nullable EnumFacing side, long rand) {
            if(side == null) return quads;
            return ImmutableList.of();
        }

        @Override
        public boolean isAmbientOcclusion() {
            return true;
        }

        @Override
        public boolean isGui3d() {
            return false;
        }

        @Override
        public boolean isBuiltInRenderer() {
            return false;
        }

        @Override
        public TextureAtlasSprite getParticleTexture() {
            return null;
        }

        @Override
        @Deprecated
        public ItemCameraTransforms getItemCameraTransforms() {
            return null;
        }

        @Override
        public ItemOverrideList getOverrides() {
            return BakedPlayerCardOverrideHandler.INSTANCE;
        }
    }

    private static final class BakedPlayerCardOverrideHandler extends ItemOverrideList {
        public static final BakedPlayerCardOverrideHandler INSTANCE = new BakedPlayerCardOverrideHandler();

        private BakedPlayerCardOverrideHandler() {
            super(ImmutableList.<ItemOverride>of());
        }

        @Override
        public IBakedModel handleItemState(IBakedModel originalModel, ItemStack stack, World world, EntityLivingBase entity) {
            boolean hasPlayer = false;

            if(stack.hasTagCompound()) {
                PlayerData data = PlayerData.readFromToNBT(stack.getTagCompound());

                if(data != null)
                    hasPlayer = true;
            }

            BakedPlayerCard model = (BakedPlayerCard)originalModel;

            IModel parent = model.parent.process(ImmutableMap.of("hasPlayer", hasPlayer ? "true" : "false"));

            Function<ResourceLocation, TextureAtlasSprite> textureGetter = new Function<ResourceLocation, TextureAtlasSprite>() {
                @Nullable
                @Override
                public TextureAtlasSprite apply(@Nullable ResourceLocation input) {
                    return Minecraft.getMinecraft().getTextureMapBlocks().getAtlasSprite(input.toString());
                }
            };

            return parent.bake(new SimpleModelState(model.transforms), model.format, textureGetter);
        }
    }

    public static final class PlayerCardModelLoader implements ICustomModelLoader {

        @Override
        public boolean accepts(ResourceLocation modelLocation) {
            //return modelLocation.getResourceDomain().equals(Reference.MOD_ID) && modelLocation.getResourcePath().equals("models/item/" + Items.PLAYER_CARD.getItem().getResourcePath());
            return false;
        }

        @Override
        public IModel loadModel(ResourceLocation modelLocation) throws Exception {
            return MODEL;
        }

        @Override
        public void onResourceManagerReload(IResourceManager resourceManager) {
        }
    }
}
