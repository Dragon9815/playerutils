package net.dragon9815.playerutils.client.gui;

import net.dragon9815.playerutils.client.gui.element.ElementBase;
import net.dragon9815.playerutils.client.gui.element.ElementButton;
import net.dragon9815.playerutils.common.reference.Reference;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.inventory.Container;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.input.Mouse;

import java.io.IOException;
import java.util.ArrayList;

public abstract class GuiBase extends GuiContainer implements IGui {
    protected final Minecraft mc = Minecraft.getMinecraft();
    public static final ResourceLocation baseTexture = new ResourceLocation(Reference.MOD_ID, "textures/gui/guiBase.png");

    private final int WIDTH = 244;
    private final int HEIGHT = 211;
    private final int TILE_WIDTH = 488;
    private final int TILE_HEIGHT = 422;

    private int x;
    private int y;
    private int width;
    private int height;

    private boolean isInit = false;
    private ArrayList<ElementBase> elements = new ArrayList<>();
    protected TileEntity tileEntity;

    public GuiBase(Container inventorySlotsIn, TileEntity tileEntity) {
        super(inventorySlotsIn);

        this.xSize = this.width = WIDTH;
        this.ySize = this.height = HEIGHT;

        this.tileEntity = tileEntity;
    }

    @Override
    public final void initGui() {
        super.initGui();

        if(!isInit) {
            onInit();
            isInit = true;
        }

        onResize(new ScaledResolution(mc));
    }

    @Override
    public void onGuiClosed() {
        super.onGuiClosed();
        onClose();
    }

    @Override
    public void onInit() {
        for(ElementBase element : elements) {
            element.onInit();
        }
    }

    @Override
    public void onResize(ScaledResolution res) {
        x = (res.getScaledWidth() - width) / 2;
        y = (res.getScaledHeight() - height) / 2;

        for(ElementBase element : elements) {
            element.onResize(res);
        }
    }

    @Override
    public void onClose() {
        for(ElementBase element : elements) {
            element.onClose();
        }
    }

    @Override
    public void drawDefaultBackground() {
        GlStateManager.pushMatrix();
        GlStateManager.translate(0, 0, -1.0f);
        super.drawDefaultBackground();
        GlStateManager.popMatrix();
    }

    @Override
    protected final void drawGuiContainerBackgroundLayer(float partialTicks, int mX, int mY) {
        GlStateManager.pushMatrix();
        GlStateManager.disableLighting();
        GlStateManager.translate(x, y, -1.0f);

        renderBackground(mX, mY, partialTicks);

        render(mX, mY, partialTicks);

        GlStateManager.enableLighting();
        GlStateManager.popMatrix();
    }

    @Override
    protected final void drawGuiContainerForegroundLayer(int mX, int mY) {
    }

    @Override
    public void renderBackground(int mX, int mY, float partTicks) {
        mc.getTextureManager().bindTexture(baseTexture);
        GuiHelper.drawTexturedModalRect(0, 0, 0, 0, width, height, TILE_WIDTH, TILE_HEIGHT, 1024, 512, 0.0f);
    }

    @Override
    public void render(int mX, int mY, float partTicks) {
        GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);

        for(ElementBase element : elements) {
            element.render(mX - x, mY - y, partTicks);
        }
    }

    @Override
    public void renderForeground(int mX, int mY, float partTicks) {
        GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);

        for(ElementBase element : elements) {
            element.renderForeground(mX - x, mY - y, partTicks);
        }
    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTicks) {
        super.drawScreen(mouseX, mouseY, partialTicks);

        GlStateManager.pushMatrix();
        GlStateManager.disableLighting();
        GlStateManager.translate(x, y, 0.0f);
        renderForeground(mouseX , mouseY, partialTicks);
        GlStateManager.enableLighting();
        GlStateManager.popMatrix();
    }

    @Override
    protected final void mouseClicked(int mouseX, int mouseY, int mouseButton) throws IOException {
        super.mouseClicked(mouseX, mouseY, mouseButton);
        onMouseClick(mouseX, mouseY, mouseButton);
    }

    @Override
    protected final void mouseClickMove(int mouseX, int mouseY, int clickedMouseButton, long timeSinceLastClick) {
        super.mouseClickMove(mouseX, mouseY, clickedMouseButton, timeSinceLastClick);
        onMouseDrag(mouseX, mouseY, clickedMouseButton);
    }

    @Override
    protected final void mouseReleased(int mouseX, int mouseY, int state) {
        super.mouseReleased(mouseX, mouseY, state);
        onMouseRelease(mouseX, mouseY, state);
    }

    @Override
    public void handleMouseInput() throws IOException {
        super.handleMouseInput();

        ScaledResolution res = new ScaledResolution(mc);
        onMouseMove((int)(Mouse.getEventX() * (res.getScaledWidth_double() / mc.displayWidth)), (int)(res.getScaledHeight() - (Mouse.getEventY() * (res.getScaledHeight_double() / mc.displayHeight))));
    }

    @Override
    public void onMouseClick(int mX, int mY, int mButton) {
        for(ElementBase element : elements) {
            if(GuiHelper.isElementHovered(element, mX - x, mY - y)) {
                element.onMouseClick(mX - x, mY - y, mButton);

                if(element instanceof ElementButton)
                    buttonClicked((ElementButton)element);
            }
        }
    }

    @Override
    public void onMouseRelease(int mX, int mY, int mButton) {
        for(ElementBase element : elements) {
            element.onMouseRelease(mX - x, mY - y, mButton);
        }
    }

    @Override
    public void onMouseDrag(int mX, int mY, int mButton) {
        for(ElementBase element : elements) {
            element.onMouseDrag(mX - x, mY - y, mButton);
        }
    }

    @Override
    public void onMouseMove(int mX, int mY) {
        for(ElementBase element : elements) {
            element.onMouseMove(mX - x, mY - y);
        }
    }

    protected final void setWidth(int width) {
        this.width = width;
    }

    @Override
    public final int getX() {
        return x;
    }

    protected final void setHeight(int height) {
        this.height = height;
    }

    @Override
    public final int getY() {
        return y;
    }

    @Override
    public final int getHeight() {
        return height;
    }

    @Override
    public final int getWidth() {
        return width;
    }

    public final void addElement(ElementBase element) {
        if(element != null)
            elements.add(element);
    }

    protected abstract void buttonClicked(ElementButton button);

    protected void drawStringScaled(String s, int x, int y, int color, float scale) {
        GlStateManager.pushMatrix();
        GlStateManager.scale(scale, scale, 1.0f);
        fontRendererObj.drawString(s, (int)(x / scale), (int)(y / scale), color);
        GlStateManager.popMatrix();
    }
}
