package net.dragon9815.playerutils.client.gui.element;

import net.dragon9815.playerutils.client.gui.GuiHelper;
import net.dragon9815.playerutils.client.gui.IGui;
import net.dragon9815.playerutils.common.reference.Reference;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.client.config.GuiUtils;

public class ElementButton extends ElementBase {
    private ResourceLocation buttonTexture = new ResourceLocation(Reference.MOD_ID, "textures/gui/button.png");

    public ElementButton(int x, int y, int width, int height) {
        super(x, y, width, height);
    }

    @Override
    public void render(int mX, int mY, float partTicks) {
        mc.getTextureManager().bindTexture(buttonTexture);

        int v = (!isEnabled()) ? 0 : ((isMouseDown()) ? 96 : ((isMouseOver()) ? 64 : 32));

        GuiHelper.drawTexturedModalRect(getX(),                  getY(),                   0,  v,      3,              3,               3, 3, 32, 128, 1.0f);
        GuiHelper.drawTexturedModalRect(getX() + 3,              getY(),                   3,  v,      getWidth() - 6, 3,               3, 3, 32, 128, 1.0f);
        GuiHelper.drawTexturedModalRect(getX() + getWidth() - 3, getY(),                   29, v,      3,              3,               3, 3, 32, 128, 1.0f);

        GuiHelper.drawTexturedModalRect(getX(),                  getY() + 3,               0,  v + 3,  3,              getHeight() - 6, 3, 3, 32, 128, 1.0f);
        GuiHelper.drawTexturedModalRect(getX() + 3,              getY() + 3,               3,  v + 3,  getWidth() - 6, getHeight() - 6, 3, 3, 32, 128, 1.0f);
        GuiHelper.drawTexturedModalRect(getX() + getWidth() - 3, getY() + 3,               29, v + 3,  3,              getHeight() - 6, 3, 3, 32, 128, 1.0f);

        GuiHelper.drawTexturedModalRect(getX(),                  getY() + getHeight() - 3, 0,  v + 29, 3,              3,               3, 3, 32, 128, 1.0f);
        GuiHelper.drawTexturedModalRect(getX() + 3,              getY() + getHeight() - 3, 3,  v + 29, getWidth() - 6, 3,               3, 3, 32, 128, 1.0f);
        GuiHelper.drawTexturedModalRect(getX() + getWidth() - 3, getY() + getHeight() - 3, 29, v + 29, 3,              3,               3, 3, 32, 128, 1.0f);
    }
}
