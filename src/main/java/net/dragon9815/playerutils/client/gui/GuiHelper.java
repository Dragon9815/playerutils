package net.dragon9815.playerutils.client.gui;

import net.dragon9815.playerutils.client.gui.element.ElementBase;
import net.dragon9815.playerutils.common.helper.LogHelper;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.VertexBuffer;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;

import java.util.List;

public class GuiHelper {

    public static boolean isPointInRegion(int pX, int pY, int rLeft, int rTop, int rRight, int rBottom) {
        return (((pX >= rLeft) && (pX <= rRight)) && ((pY >= rTop) && (pY <= rBottom)));
    }

    public static boolean isElementHovered(ElementBase element, int mX, int mY) {
        return isPointInRegion(mX, mY, element.getX(), element.getY(), element.getX() + element.getWidth(), element.getY() + element.getHeight());
    }

    public static void drawTexturedModalRect(float x, float y, float u, float v, float width, float height, float tileWidth, float tileHeight, int textureWidth, int textureHeight, float zLevel)
    {
        float uScale = 1f / textureWidth;
        float vScale = 1f / textureHeight;
        Tessellator tessellator = Tessellator.getInstance();
        VertexBuffer wr = tessellator.getBuffer();

        wr.begin(7, DefaultVertexFormats.POSITION_TEX);
        wr.pos(x        , y + height, zLevel).tex( u              * uScale, ((v + tileHeight) * vScale)).endVertex();
        wr.pos(x + width, y + height, zLevel).tex((u + tileWidth) * uScale, ((v + tileHeight) * vScale)).endVertex();
        wr.pos(x + width, y         , zLevel).tex((u + tileWidth) * uScale, ( v               * vScale)).endVertex();
        wr.pos(x        , y         , zLevel).tex( u              * uScale, ( v               * vScale)).endVertex();
        tessellator.draw();
    }

    public static void drawTexturedModalRect(float x, float y, float u, float v, float width, float height, float zLevel)
    {
        drawTexturedModalRect(x, y, u, v, width, height, width, height, 256, 256, zLevel);
    }

    public static void drawProgressBarVertical(float max, float current, int x, int y, int u, int v, int width, int height, int tileWidth, int tileHeight, int textureWidth, int textureHeight, float zLevel) {
        float height1 = ((height * current) / max);
        float tileHeight1 = ((tileHeight * current) / max);
        float y1 = (y + height) - height1;
        float v1 = (v + tileHeight) - tileHeight1;

        drawTexturedModalRect(x, y1, u, v1, width, height1, tileWidth, tileHeight1, textureWidth, textureHeight, zLevel);
    }

    public static void drawProgressBarHorizontal(int max, int current, int x, int y, int u, int v, int width, int height, int tileWidth, int tileHeight, int textureWidth, int textureHeight, float zLevel) {
        int x1 = ((x + width) * current) / max;
        int u1 = ((u + tileWidth) * current) / max;

        drawTexturedModalRect(x1, y, u1, v, width, height, tileWidth, tileHeight, textureWidth, textureHeight, zLevel);
    }
}
