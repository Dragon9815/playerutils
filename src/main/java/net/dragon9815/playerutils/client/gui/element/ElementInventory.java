package net.dragon9815.playerutils.client.gui.element;

import net.dragon9815.playerutils.client.gui.IGui;
import net.dragon9815.playerutils.common.util.Point2D;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.Slot;
import net.minecraft.util.math.Vec2f;

import java.util.ArrayList;

public class ElementInventory extends ElementBase {

    private Container container;
    private int startSlot;
    private int endSlot;
    private Point2D[] originalSlotPositions;

    public ElementInventory(Container container) {
        this(container, 0, container.getInventory().size() - 1);
    }

    public ElementInventory(Container container, int startSlot, int endSlot) {
        super(0, 0, 0, 0);

        originalSlotPositions = new Point2D[container.inventorySlots.size()];
        this.startSlot = startSlot;
        this.endSlot = endSlot;
        this.container = container;

        for(int i = 0; i < container.inventorySlots.size(); i++) {
            Slot slot = container.getSlot(i);
            originalSlotPositions[i] = new Point2D(slot.xDisplayPosition, slot.yDisplayPosition);
        }

        for(int i = 0; i < container.inventorySlots.size(); i++) {
            if(i < startSlot || i > endSlot)
                container.getSlot(i).xDisplayPosition = -100;
        }
    }

    @Override
    public void onClose() {
        super.onClose();

        for(int i = 0; i < container.inventorySlots.size(); i++) {
            Slot slot = container.getSlot(i);
            Point2D pos = originalSlotPositions[i];

            slot.xDisplayPosition = pos.getX();
            slot.yDisplayPosition = pos.getY();
        }
    }

    @Override
    public void render(int mX, int mY, float partTicks) {
        // Nothing to render because I let standard minecraft code handle it.
    }
}
