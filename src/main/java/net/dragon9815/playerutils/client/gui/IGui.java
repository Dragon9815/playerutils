package net.dragon9815.playerutils.client.gui;

import net.minecraft.client.gui.ScaledResolution;

public interface IGui {
    void onInit();
    void onResize(ScaledResolution res);
    void onClose();

    void renderBackground(int mX, int mY, float partTicks);
    void render(int mX, int mY, float partTicks);
    void renderForeground(int mX, int mY, float partTicks);

    void onMouseClick(int mX, int mY, int mButton);
    void onMouseRelease(int mX, int mY, int mButton);
    void onMouseDrag(int mX, int mY, int mButton);
    void onMouseMove(int mX, int mY);

    int getX();
    int getY();
    int getHeight();
    int getWidth();
}
