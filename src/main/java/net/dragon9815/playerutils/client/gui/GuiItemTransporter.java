package net.dragon9815.playerutils.client.gui;

import cofh.api.energy.IEnergyHandler;
import net.dragon9815.playerutils.client.gui.element.ElementButton;
import net.dragon9815.playerutils.client.gui.element.ElementEnergyBar;
import net.dragon9815.playerutils.client.gui.element.ElementInventory;
import net.dragon9815.playerutils.common.capabilities.upgrade.CapabilityUpgradeHandler;
import net.dragon9815.playerutils.common.capabilities.upgrade.IUpgradeHandler;
import net.dragon9815.playerutils.common.helper.StringHelper;
import net.dragon9815.playerutils.common.inventory.container.ContainerItemTransporter;
import net.dragon9815.playerutils.common.reference.Reference;
import net.dragon9815.playerutils.common.reference.UpgradeEffects;
import net.dragon9815.playerutils.common.tileentity.machines.TileEntityItemTransporter;
import net.dragon9815.playerutils.common.util.EnumUpgrade;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.util.ResourceLocation;

public class GuiItemTransporter extends GuiBase {
    public static final int ID = 0;
    private static final ResourceLocation overlay = new ResourceLocation(Reference.MOD_ID, "textures/gui/itemTransporter_overlay.png");

    private IEnergyHandler energyHandler = null;
    private IUpgradeHandler upgradeHandler = null;

    public GuiItemTransporter(TileEntityItemTransporter tileEntity, InventoryPlayer inventoryPlayer) {
        super(new ContainerItemTransporter(inventoryPlayer, tileEntity), tileEntity);

        energyHandler = tileEntity;

        if(tileEntity.hasCapability(CapabilityUpgradeHandler.UPGRADE_HANDLER_CAPABILITY, null)) {
            upgradeHandler = tileEntity.getCapability(CapabilityUpgradeHandler.UPGRADE_HANDLER_CAPABILITY, null);
        }
    }

    @Override
    public void onInit() {
        super.onInit();

        addElement(new ElementInventory(inventorySlots));
        //addElement(new ElementButton(10, 10, 100, 20));
        if(energyHandler != null)
           addElement(new ElementEnergyBar(8, 128, 16, 69, energyHandler, null));
    }

    @Override
    public void renderBackground(int mX, int mY, float partTicks) {
        super.renderBackground(mX, mY, partTicks);

        mc.getTextureManager().bindTexture(overlay);
        GuiHelper.drawTexturedModalRect(0, 0, 0, 0, getWidth(), getHeight(), 488, 422, 512, 512, 0.0f);
    }

    @Override
    public void render(int mX, int mY, float partTicks) {
        super.render(mX, mY, partTicks);

        mc.getTextureManager().bindTexture(baseTexture);

        GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
        GlStateManager.enableBlend();
        GuiHelper.drawTexturedModalRect(8, 154, 500, 150, 16, 16, 32, 32, 1024, 512, 0.0f);

        // Playercard-slot
        String slotString = StringHelper.translateToLocal("label.playerutils.player_slot");
        fontRendererObj.drawString(slotString, 31 - fontRendererObj.getStringWidth(slotString) / 2, 40, 0x404040);

        // buffer
        fontRendererObj.drawString(StringHelper.translateToLocal("label.playerutils.buffer"), 65, 12, 0x404040);
        // details
        fontRendererObj.drawString(StringHelper.translateToLocal("label.playerutils.info"), 162, 12, 0x404040);

        drawStringScaled(String.format("%s:", StringHelper.translateToLocal("label.playerutils.speed")), 165, 30, 0x404040, 0.7f);
        drawStringScaled(String.format("- %d/tick", UpgradeEffects.Speed.levels[upgradeHandler.getUpgradeLevel(EnumUpgrade.SPEED)]), 165, 36, 0x404040, 0.7f);
        drawStringScaled(String.format("%s:", StringHelper.translateToLocal("label.playerutils.range")), 165, 45, 0x404040, 0.7f);
        int range = UpgradeEffects.Range.levels[upgradeHandler.getUpgradeLevel(EnumUpgrade.RANGE)];
        drawStringScaled(String.format("- %s", (range == UpgradeEffects.Range.INFINITE) ? StringHelper.translateToLocal("label.playerutils.infinite") : String.format("%d", range)), 165, 51, 0x404040, 0.7f);
        drawStringScaled(String.format("%s:", StringHelper.translateToLocal("label.playerutils.multidimensional")), 165, 60, 0x404040, 0.7f);
        //drawStringScaled(String.format("- %s", upgradeHandler.hasUpgrade(EnumUpgrade.MULTIDIMENSIONAL) ? StringHelper.translateToLocal("label.playerutils.all_dimensions") : StringHelper.translateToLocal("label.playerutils.overworld_only")), 165, 66, 0x404040, 0.7f);


        if(upgradeHandler.hasUpgrade(EnumUpgrade.MULTIDIMENSIONAL)) {
            if (range != UpgradeEffects.Range.INFINITE) {
                drawStringScaled(String.format("- %s", StringHelper.translateToLocal("label.playerutils.overworld_only")), 165, 66, 0x404040, 0.7f);
                drawStringScaled(StringHelper.translateToLocal("label.playerutils.range_needed"), 165, 75, 0x800000, 0.7f);
            }
            else {
                drawStringScaled(String.format("- %s", StringHelper.translateToLocal("label.playerutils.all_dimensions")), 165, 66, 0x404040, 0.7f);
            }
        }
        else {
            drawStringScaled(String.format("- %s", StringHelper.translateToLocal("label.playerutils.overworld_only")), 165, 66, 0x404040, 0.7f);
        }
    }

    @Override
    protected void buttonClicked(ElementButton button) {

    }


}
