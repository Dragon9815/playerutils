package net.dragon9815.playerutils.client.gui.element;

import net.dragon9815.playerutils.client.gui.GuiHelper;
import net.dragon9815.playerutils.client.gui.IGui;
import net.dragon9815.playerutils.common.helper.LogHelper;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.ScaledResolution;

public abstract class ElementBase implements IGui {
    protected final Minecraft mc = Minecraft.getMinecraft();

    // Dimensions
    private int x;
    private int y;
    private int width;
    private int height;

    private boolean mouseDown;
    private boolean mouseOver;
    private boolean enabled;

    public ElementBase(int x, int y, int width, int height) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.enabled = true;
    }

    @Override
    public void onInit() {
    }

    @Override
    public void onResize(ScaledResolution res) {
    }

    @Override
    public void onClose() {
    }

    @Override
    public void onMouseClick(int mX, int mY, int mButton) {
        if(mButton == 0) {
            mouseDown = true;
            LogHelper.info("Click, x: " + mX + ", y: " + mY);
        }
    }

    @Override
    public void onMouseRelease(int mX, int mY, int mButton) {
        if(mButton == 0)
            mouseDown = false;
    }

    @Override
    public void onMouseDrag(int mX, int mY, int mButton) {
    }

    @Override
    public void onMouseMove(int mX, int mY) {
        mouseOver = isHovered(mX, mY);
    }

    @Override
    public int getX() {
        return x;
    }

    protected void setX(int x) {
        this.x = x;
    }

    @Override
    public int getY() {
        return y;
    }

    protected void setY(int y) {
        this.y = y;
    }

    @Override
    public int getHeight() {
        return height;
    }

    @Override
    public int getWidth() {
        return width;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    protected boolean isMouseDown() {
        return mouseDown;
    }

    protected boolean isMouseOver() {
        return mouseOver;
    }

    protected boolean isHovered(int mX, int mY) {
        return GuiHelper.isPointInRegion(mX, mY, getX(), getY(), getX() + getWidth(), getY() + getHeight());
    }

    @Override
    public final void renderBackground(int mX, int mY, float partTicks) {}

    @Override
    public void renderForeground(int mX, int mY, float partTicks) {}
}
