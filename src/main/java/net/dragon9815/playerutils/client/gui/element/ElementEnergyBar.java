package net.dragon9815.playerutils.client.gui.element;

import cofh.api.energy.IEnergyHandler;
import cofh.api.energy.IEnergyStorage;
import net.dragon9815.playerutils.client.gui.GuiBase;
import net.dragon9815.playerutils.client.gui.GuiHelper;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.util.EnumFacing;
import net.minecraftforge.fml.client.config.GuiUtils;

import java.util.Arrays;
import java.util.Collections;

public class ElementEnergyBar extends ElementBase {

    private IEnergyHandler energyHandler;
    private EnumFacing side;

    private int energyStoredPrev;
    private int change = Integer.MAX_VALUE;
    private float prevPartTick;

    private ScaledResolution resolution;

    public ElementEnergyBar(int x, int y, int width, int height, IEnergyHandler energyHandler, EnumFacing side) {
        super(x, y, width, height);

        this.energyHandler = energyHandler;
        this.side = side;
    }

    @Override
    public void onResize(ScaledResolution res) {
        super.onResize(res);

        resolution = res;
    }

    @Override
    public void render(int mX, int mY, float partTicks) {
        GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);

        mc.getTextureManager().bindTexture(GuiBase.baseTexture);

        if(partTicks < prevPartTick || change == Integer.MAX_VALUE) {
            change = energyHandler.getEnergyStored(side) - energyStoredPrev;
            energyStoredPrev = energyHandler.getEnergyStored(side);
        }

        GuiHelper.drawProgressBarVertical(energyHandler.getMaxEnergyStored(side), energyHandler.getEnergyStored(side) + change, getX(), getY(), 500, 0, getWidth(), getHeight(), 32, 138, 1024, 512, 0.0f);

        prevPartTick = partTicks;
    }

    @Override
    public void renderForeground(int mX, int mY, float partTicks) {
        super.renderForeground(mX, mY, partTicks);

        if(isMouseOver())
            GuiUtils.drawHoveringText(Collections.singletonList(String.format("%d/%d RF", energyHandler.getEnergyStored(side), energyHandler.getMaxEnergyStored(side))), mX, mY, resolution.getScaledWidth(), resolution.getScaledHeight(), 1000, mc.fontRendererObj);
    }
}
