package net.dragon9815.playerutils.common.reference;

public class UpgradeEffects {
    public static class Range {
        public static final int SMALL = 8;
        public static final int MEDIUM = 32;
        public static final int LARGE = 256;
        public static final int INFINITE = -1;

        public static final int[] levels = {
            SMALL, MEDIUM, LARGE, INFINITE
        };
    }

    public static class Speed {
        public static final int SLOW = 2;
        public static final int MEDIUM = 8;
        public static final int FAST = 32;
        public static final int VERY_FAST = 64;

        public static final int[] levels = {
                SLOW, MEDIUM, FAST, VERY_FAST
        };
    }
}
