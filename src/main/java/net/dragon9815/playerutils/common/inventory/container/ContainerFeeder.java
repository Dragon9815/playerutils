package net.dragon9815.playerutils.common.inventory.container;

import net.dragon9815.playerutils.common.inventory.slot.SlotPlayer;
import net.dragon9815.playerutils.common.tileentity.machines.TileEntityFeeder;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraftforge.items.SlotItemHandler;

public class ContainerFeeder extends ContainerBase {

    public ContainerFeeder(InventoryPlayer inventoryPlayer, TileEntityFeeder tileEntity) {
        // 0 - 35
        super(inventoryPlayer, tileEntity);

        // 36 - 38
        addUpgradeSlots(3);

        // Add buffer slots
        // 39 - 47
        for(int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                this.addSlotToContainer(new SlotItemHandler(itemHandler, i * 3 + j, 66 + 18 * j, 25 + i * 18));
            }
        }

        // 48
        this.addSlotToContainer(new SlotPlayer(playerHandler, 0, 22, 52));
    }

    @Override
    public ItemStack transferStackInSlot(EntityPlayer playerIn, int index) {
        Slot slot = inventorySlots.get(index);
        ItemStack stack = null;

        if(slot != null && slot.getHasStack()) {
            stack = slot.getStack();

            ItemStack prevStack = stack.copy();

            // Player inventory
            if (index >= 0 && index < 36) {
                // Try Player-Card Slot
                if(!mergeItemStack(stack, 48, 49, false)) {
                    // Try upgrade slots
                    if (mergeItemStack(stack, 36, 39, false)) {
                        if(stack.stackSize <= 0)
                            slot.putStack(null);

                        return null;
                    }
                    // Try buffer slots
                    if (!mergeItemStack(stack, 39, 48, false)) {
                        // Do nothing
                        return null;
                    }
                }
            }
            // Upgrade Slot
            else if (index >= 36 && index < 39) {
                // Try player inventory
                if (!mergeItemStack(stack, 0, 36, false)) {
                    // Do nothing
                    return null;
                }
            }
            // Buffer Slots
            else if(index >= 39 && index < 48) {
                // Try player inventory
                if(!mergeItemStack(stack, 0, 36, false))
                    return null;
            }
            // Player-Card Slot
            else if(index == 48) {
                // Try player inventory
                if(!mergeItemStack(stack, 0, 36, false))
                    return null;
            }

            if(stack.stackSize <= 0) {
                slot.putStack(null);
                stack = null;
            }

            slot.onSlotChange(prevStack, stack);
        }

        return stack;
    }
}
