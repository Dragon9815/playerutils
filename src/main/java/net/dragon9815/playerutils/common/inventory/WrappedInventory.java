/*
 * This file is part of PlayerUtils
 * Copyright (C) 2016 Dragon9815 and others.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package net.dragon9815.playerutils.common.inventory;

import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;

public class WrappedInventory {
    private ItemStack[] stacks;

    public WrappedInventory(int size) {
        super();
        stacks = new ItemStack[size];
    }

    public static WrappedInventory fromIInventory(IInventory inventory) {
        WrappedInventory ret = new WrappedInventory(inventory.getSizeInventory());

        for (int i = 0; i < inventory.getSizeInventory(); i++) {
            ret.stacks[i] = ItemStack.copyItemStack(inventory.getStackInSlot(i));
        }

        return ret;
    }

    public boolean compare(WrappedInventory inventory) {
        for(int i = 0; i < inventory.stacks.length; i++) {
            if(!ItemStack.areItemStacksEqual(inventory.stacks[i], stacks[i]))
                return false;
        }

        return true;
    }
}
