/*
 * This file is part of PlayerUtils
 * Copyright (C) 2016 Dragon9815 and others.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package net.dragon9815.playerutils.common.inventory.slot;

import net.dragon9815.playerutils.common.capabilities.upgrade.IUpgradeHandler;
import net.dragon9815.playerutils.common.capabilities.upgrade.IUpgradeHandlerModifyable;
import net.dragon9815.playerutils.common.capabilities.upgrade.Upgrade;
import net.dragon9815.playerutils.common.helper.LogHelper;
import net.dragon9815.playerutils.common.item.Items;
import net.dragon9815.playerutils.common.util.EnumUpgrade;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.InventoryBasic;
import net.minecraft.item.ItemStack;

public class SlotUpgrade extends SlotBase {
    private static IInventory emptyInventory = new InventoryBasic("[Null]", true, 0);
    private final IUpgradeHandlerModifyable upgradeHandler;
    private final int index;

    public SlotUpgrade(IUpgradeHandlerModifyable upgradeHandler, int index, int xPosition, int yPosition)
    {
        super(emptyInventory, index, xPosition, yPosition);
        this.upgradeHandler = upgradeHandler;
        this.index = index;
    }


    @Override
    public boolean isItemValid(ItemStack stack)
    {
        if(stack == null)
            return false;

        if(!stack.getItem().equals(Items.UPGRADE.getItem()))
            return false;

        EnumUpgrade type = EnumUpgrade.byMeta(stack.getMetadata());

        return upgradeHandler.getUpgradeLevel(type) < type.getMaxLevel();
    }

    @Override
    public ItemStack getStack()
    {
        return upgradeHandler.getUpgradeInSlot(index);
    }

    @Override
    public void putStack(ItemStack stack)
    {
        upgradeHandler.clearSlot(index);

        if(stack != null) {
            upgradeHandler.setUpgradeInSlot(index, Upgrade.fromItemStack(stack));
        }

        this.onSlotChanged();
    }

    @Override
    public int getItemStackLimit(ItemStack stack)
    {
        return EnumUpgrade.byMeta(stack.getMetadata()).getMaxLevel();
    }

    @Override
    public boolean canTakeStack(EntityPlayer playerIn) {
        return true;
    }

    @Override
    public ItemStack decrStackSize(int amount)
    {
        return upgradeHandler.extractUpgrade(index, amount, false);
    }

    @Override
    public void addToSlot(int amount) {
        if(getStack() != null) {
            ItemStack stack = getStack().copy();
            stack.stackSize = 1;
            upgradeHandler.insertUpgrade(index, stack, false);
        }
    }

    @Override
    public void onSlotChange(ItemStack p_75220_1_, ItemStack p_75220_2_) {
    }
}
