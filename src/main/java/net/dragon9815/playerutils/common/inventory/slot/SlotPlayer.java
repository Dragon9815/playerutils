package net.dragon9815.playerutils.common.inventory.slot;

import net.dragon9815.playerutils.common.capabilities.player.IPlayerHandler;
import net.dragon9815.playerutils.common.helper.LogHelper;
import net.dragon9815.playerutils.common.item.Items;
import net.dragon9815.playerutils.common.util.PlayerData;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.InventoryBasic;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;

import javax.annotation.Nullable;

public class SlotPlayer extends SlotBase {
    private static IInventory emptyInventory = new InventoryBasic("[Null]", true, 0);

    private IPlayerHandler playerHandler;
    private boolean isCardInSlot = false;

    public SlotPlayer(IPlayerHandler playerHandler, int index, int xPosition, int yPosition) {
        super(emptyInventory, index, xPosition, yPosition);
        this.playerHandler = playerHandler;
    }

    @Nullable
    @Override
    public ItemStack getStack() {
        ItemStack stack = null;

        if(playerHandler.getPlayer() != null && playerHandler.getPlayer().isValid()) {
            stack = new ItemStack(Items.PLAYER_CARD.getItem(), 1, 1);
            NBTTagCompound tagCompound = new NBTTagCompound();
            playerHandler.getPlayer().writeToNBT(tagCompound);
            stack.setTagCompound(tagCompound);
        }
        else if(isCardInSlot){
            stack = new ItemStack(Items.PLAYER_CARD.getItem(), 1, 0);
        }

        return stack;
    }

    @Override
    public boolean isItemValid(@Nullable ItemStack stack) {
        return stack == null || (stack.getItem().equals(Items.PLAYER_CARD.getItem()));
    }

    @Override
    public void putStack(@Nullable ItemStack stack) {
        if(stack == null) {
            isCardInSlot = false;
            playerHandler.setPlayer(null);
        }
        else {
            isCardInSlot = true;

            if(stack.hasTagCompound()) {
                PlayerData data = PlayerData.readFromToNBT(stack.getTagCompound());

                if(data.isValid())
                    playerHandler.setPlayer(data);
                else {
                    playerHandler.setPlayer(null);
                    LogHelper.warn("Player data invalid on putStack!");
                }
            }
            else {
                LogHelper.fatal("No NBT data on the stack with meta 1. This should not happen!");
            }
        }

        onSlotChanged();
    }

    @Override
    public int getSlotStackLimit() {
        return 1;
    }

    @Override
    public boolean canTakeStack(EntityPlayer playerIn) {
        return true;
    }

    @Override
    public ItemStack decrStackSize(int amount) {
        ItemStack stack = getStack();
        putStack(null);

        return stack;
    }

    @Override
    public void onSlotChange(ItemStack p_75220_1_, ItemStack p_75220_2_) {
    }
}
