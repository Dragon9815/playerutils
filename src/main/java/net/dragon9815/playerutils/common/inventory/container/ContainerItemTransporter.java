/*
 * This file is part of PlayerUtils
 * Copyright (C) 2016 Dragon9815 and others.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package net.dragon9815.playerutils.common.inventory.container;

import com.google.common.collect.Sets;
import net.dragon9815.playerutils.common.inventory.slot.SlotPlayer;
import net.dragon9815.playerutils.common.tileentity.machines.TileEntityItemTransporter;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.ClickType;
import net.minecraft.inventory.Slot;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.items.SlotItemHandler;

import javax.annotation.Nullable;
import java.util.Set;

public class ContainerItemTransporter extends ContainerBase {

    public ContainerItemTransporter(InventoryPlayer invPlayer, TileEntityItemTransporter tileEntity) {
        // 0 - 35
        super(invPlayer, tileEntity);

        // 36 - 38
        addUpgradeSlots(3);

        // Add buffer slots
        // 39 - 58
        for(int i = 0; i < 4; i++) {
            for (int j = 0; j < 5; j++) {
                this.addSlotToContainer(new SlotItemHandler(itemHandler, i * 5 + j, 66 + 18 * j, 25 + i * 18));
            }
        }

        // 59
        this.addSlotToContainer(new SlotPlayer(playerHandler, 0, 22, 52));
    }

    @Override
    public ItemStack transferStackInSlot(EntityPlayer playerIn, int index) {
        Slot slot = inventorySlots.get(index);
        ItemStack stack = null;

        if(slot != null && slot.getHasStack()) {
            stack = slot.getStack();

            ItemStack prevStack = stack.copy();

            // Player inventory
            if (index >= 0 && index < 36) {
                // Try Player-Card Slot
                if(!mergeItemStack(stack, 59, 60, false)) {
                    // Try upgrade slots
                    if (mergeItemStack(stack, 36, 39, false)) {
                        if(stack.stackSize <= 0)
                            slot.putStack(null);

                        return null;
                    }
                    // Try buffer slots
                    if (!mergeItemStack(stack, 39, 59, false)) {
                        // Do nothing
                        return null;
                    }
                }
            }
            // Upgrade Slot
            else if (index >= 36 && index < 39) {
                // Try player inventory
                if (!mergeItemStack(stack, 0, 36, false)) {
                    // Do nothing
                    return null;
                }
            }
            // Buffer Slots
            else if(index >= 39 && index < 59) {
                // Try player inventory
                if(!mergeItemStack(stack, 0, 36, false))
                    return null;
            }
            // Player-Card Slot
            else if(index == 59) {
                // Try player inventory
                if(!mergeItemStack(stack, 0, 36, false))
                    return null;
            }

            if(stack.stackSize <= 0) {
                slot.putStack(null);
                stack = null;
            }

            slot.onSlotChange(prevStack, stack);
        }

        return stack;
    }
}
