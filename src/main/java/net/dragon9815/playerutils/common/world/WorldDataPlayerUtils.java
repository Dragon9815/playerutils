/*
 * This file is part of PlayerUtils
 * Copyright (C) 2016 Dragon9815 and others.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package net.dragon9815.playerutils.common.world;

import net.dragon9815.playerutils.common.util.PlayerData;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.world.World;
import net.minecraft.world.WorldSavedData;

import java.util.ArrayList;

public class WorldDataPlayerUtils extends WorldSavedData {
    public static final String IDENTIFIER = "playerutils";
    public ArrayList<PlayerData> players;

    public WorldDataPlayerUtils() {
        this(IDENTIFIER);
    }

    public WorldDataPlayerUtils(String name) {
        super(name);
        players = new ArrayList<>();
    }

    @Override
    public void readFromNBT(NBTTagCompound nbt) {
        NBTTagList tagList = nbt.getTagList("players", 10);
        for(int i = 0; i < tagList.tagCount(); i++) {
            NBTTagCompound tagCompound = tagList.getCompoundTagAt(i);

            players.add(PlayerData.readFromToNBT(tagCompound));
        }
    }

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound nbt) {
        NBTTagList tagList = new NBTTagList();
        for(PlayerData player : players) {
            NBTTagCompound tagCompound = new NBTTagCompound();

            player.writeToNBT(tagCompound);

            tagList.appendTag(tagCompound);
        }

        nbt.setTag("players", tagList);

        return nbt;
    }

    public void setPlayers(ArrayList<PlayerData> players) {
        this.players = players;
        if(this.players == null)
            this.players = new ArrayList<>();
        markDirty();
    }

    public static WorldDataPlayerUtils getData(World world) {
        WorldDataPlayerUtils data = (WorldDataPlayerUtils)world.loadItemData(WorldDataPlayerUtils.class, IDENTIFIER);
        if(data == null) {
            data = new WorldDataPlayerUtils();
            world.setItemData(IDENTIFIER, data);
        }
        return data;
    }
}
