/*
 * This file is part of PlayerUtils
 * Copyright (C) 2016 Dragon9815 and others.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package net.dragon9815.playerutils.common.world;

import net.minecraft.block.state.IBlockState;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.chunk.IChunkGenerator;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.feature.WorldGenMinable;
import net.minecraftforge.fml.common.IWorldGenerator;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class WorldGen implements IWorldGenerator {
    private static List<OreGenDataHolder> listOreGen = new ArrayList<OreGenDataHolder>();

    public static void addOreGen(IBlockState ore, int minY, int maxY, int veinsPerChunk, int maxBlocksPerVein) {
        listOreGen.add(new OreGenDataHolder(ore, minY, maxY, veinsPerChunk, maxBlocksPerVein));
    }

    @Override
    public void generate(Random random, int chunkX, int chunkZ, World world, IChunkGenerator chunkGenerator, IChunkProvider chunkProvider) {
        switch (world.provider.getDimension()) {
            case 0:
                generateSurface(world, random, chunkX * 16, chunkZ * 16);
        }
    }

    private void generateSurface(World world, Random rand, int chunkX, int chunkZ) {
        for (OreGenDataHolder oreData : listOreGen) {
            generateOre(world, rand, chunkX, chunkZ, oreData);
        }
    }

    private void generateOre(World world, Random random, int chunkX, int chunkZ, OreGenDataHolder oreData) {
        for (int i = 0; i < oreData.veinsPerChunk; i++) {
            int firstBlockXCoord = chunkX + random.nextInt(16);
            int firstBlockZCoord = chunkZ + random.nextInt(16);

            int y = random.nextInt(oreData.maxY - oreData.minY) + oreData.minY;
            BlockPos pos = new BlockPos(firstBlockXCoord, y, firstBlockZCoord);

            (new WorldGenMinable(oreData.ore, oreData.maxBlocksPerVein)).generate(world, random, pos);
        }
    }

    private static class OreGenDataHolder {
        IBlockState ore;
        int minY;
        int maxY;
        int veinsPerChunk;
        int maxBlocksPerVein;

        public OreGenDataHolder(IBlockState ore, int minY, int maxY, int veinsPerChunk, int maxBlocksPerVein) {
            this.ore = ore;
            this.minY = minY;
            this.maxY = maxY;
            this.veinsPerChunk = veinsPerChunk;
            this.maxBlocksPerVein = maxBlocksPerVein;
        }
    }
}
