/*
 * This file is part of PlayerUtils
 * Copyright (C) 2016 Dragon9815 and others.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package net.dragon9815.playerutils.common.util;

import net.dragon9815.playerutils.common.capabilities.upgrade.Upgrade;
import net.minecraft.item.Item;

public enum EnumUpgrade {
    RANGE(0, 3, "range"),
    RFTIER(1, 3, "rftier"),
    EUTIER(2, 3, "eutier"),
    MULTIDIMENSIONAL(3, 1, "multidim", new Upgrade(RANGE, 3)),
    FOOD_EFFICIENCY(4, 1, "food_eff"),
    SPEED(5, 3, "speed");

    private static EnumUpgrade[] metaLookup;

    static {
        metaLookup = new EnumUpgrade[values().length];
        for(EnumUpgrade upgrade : values()) {
            metaLookup[upgrade.meta] = upgrade;
        }
    }

    public static EnumUpgrade byMeta(int meta) {
        return metaLookup[meta];
    }

    private final int maxLevel;
    private final String name;
    private final Upgrade[] reqs;
    private final int meta;

    EnumUpgrade(int meta, int maxLevel, String name, Upgrade... reqs) {
        this.meta = meta;
        this.maxLevel = maxLevel;
        this.name = name;
        this.reqs = reqs;
    }

    public int getMaxLevel() { return maxLevel; }

    public String getName() { return name; }

    public Upgrade[] getReqs() { return reqs; }

    public int getMeta() { return meta; }

    @Override
    public String toString() { return getName(); }


}
