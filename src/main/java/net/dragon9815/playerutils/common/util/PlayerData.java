/*
 * This file is part of PlayerUtils
 * Copyright (C) 2016 Dragon9815 and others.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package net.dragon9815.playerutils.common.util;

import net.dragon9815.playerutils.common.helper.LogHelper;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;

import java.util.UUID;

public class PlayerData {
    public String name;
    public UUID uuid;
    public boolean isOnline;

    public PlayerData() {
        this("", null, false);
    }

    public PlayerData(String name, UUID uuid, boolean isOnline) {
        this.name = name;
        this.uuid = uuid;
        this.isOnline = isOnline;
    }

    public boolean isValid() {
        return uuid != null && !name.equals("");
    }

    public NBTTagCompound writeToNBT(NBTTagCompound tagCompound) {
        tagCompound.setString("name", name);
        if(uuid != null)
            tagCompound.setString("uuid", uuid.toString());
        tagCompound.setBoolean("isOnline", isOnline);

        return tagCompound;
    }

    public static PlayerData fromPlayer(EntityPlayer player) {
        return new PlayerData(player.getName(), player.getUniqueID(), true);
    }

    public static PlayerData readFromToNBT(NBTTagCompound tagCompound) {
        PlayerData data = new PlayerData();
        data.name = tagCompound.getString("name");
        try {
            if(tagCompound.hasKey("uuid"))
                data.uuid = UUID.fromString(tagCompound.getString("uuid"));
        }
        catch (IllegalArgumentException e) {
            LogHelper.warn("The UUID thingy failed, this shouldn't happen!");
            e.printStackTrace();
        }
        data.isOnline = tagCompound.getBoolean("isOnline");

        return data;
    }

}
