package net.dragon9815.playerutils.common.util;

import net.dragon9815.playerutils.common.block.BlockBase;
import net.dragon9815.playerutils.common.helper.LogHelper;
import net.dragon9815.playerutils.common.item.ItemBase;
import net.dragon9815.playerutils.common.reference.Reference;
import net.minecraft.block.Block;
import net.minecraft.block.properties.IProperty;
import net.minecraft.item.ItemBlock;
import net.minecraftforge.fml.common.registry.GameRegistry;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;

public class Platform {

    @SuppressWarnings("unchecked")
    private static <T extends Comparable<T>> String getPropertyValue(IProperty<T> property, Comparable<?> comparable) {
        return property.getName((T)comparable);
    }

    public static String buildPropertyString(Map<IProperty<?>, Comparable<?>> properties) {
        StringBuilder builder = new StringBuilder();

        for(Map.Entry<IProperty<?>, Comparable<?>> entry : properties.entrySet()) {
            if(builder.length() > 0)
                builder.append(',');

            IProperty<?> prop = entry.getKey();
            builder.append(prop.getName());
            builder.append("=");
            builder.append(getPropertyValue(prop, entry.getValue()));
        }

        if(builder.length() == 0)
            builder.append("inventory");

        return builder.toString();
    }

    public static BlockBase registerBlock(Class<? extends BlockBase> blockClass, Class<? extends ItemBlock> itemblockClass) {
        ItemBlock itemblock = null;
        BlockBase block = null;

        try {
            block = blockClass.newInstance();
            itemblock = itemblockClass.getConstructor(Block.class).newInstance(block);

            block.setRegistryName(Reference.MOD_ID, block.getInternalName());
            block.setUnlocalizedName(block.getRegistryName().toString());
            itemblock.setRegistryName(Reference.MOD_ID, block.getInternalName());
            itemblock.setUnlocalizedName(block.getRegistryName().toString());

            GameRegistry.register(itemblock);
            GameRegistry.register(block);

            LogHelper.info("Registered Block [" + blockClass.getCanonicalName() + "]");

            return block;
        }
        catch (Exception e) {
            LogHelper.fatal("Exception raised while registering Block [" + blockClass.getCanonicalName() + "]!");
            e.printStackTrace();
        }

        return null;
    }

    public static ItemBase registerItem(Class<? extends ItemBase> itemClass) {
        ItemBase item = null;

        try {
            item = itemClass.newInstance();

            item.setUnlocalizedName(item.getInternalName());
            item.setRegistryName(Reference.MOD_ID, item.getInternalName());

            GameRegistry.register(item);

            LogHelper.info("Registered Item [" + itemClass.getCanonicalName() + "]");

            return item;
        }
        catch (Exception e) {
            LogHelper.fatal("Exception raised while registering Item [" + itemClass.getCanonicalName() + "]!");
            e.printStackTrace();
        }

        return null;
    }
}
