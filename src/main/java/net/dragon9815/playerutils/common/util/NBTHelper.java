/*
 * This file is part of PlayerUtils
 * Copyright (C) 2016 Dragon9815 and others.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package net.dragon9815.playerutils.common.util;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.math.BlockPos;

import javax.annotation.Nonnull;

public class NBTHelper {

    /**
     * This Method saves a BlockPos to an NBTTagCompound
     * @param pos The position to save, should not be null
     * @return The NBTTagCompound with the data
     */
    public static NBTTagCompound writeBlockPos(@Nonnull BlockPos pos) {
        NBTTagCompound tagCompound = new NBTTagCompound();
        tagCompound.setInteger("x", pos.getX());
        tagCompound.setInteger("y", pos.getY());
        tagCompound.setInteger("z", pos.getZ());
        return tagCompound;
    }

    public static NBTTagCompound writeDimensionalBlockPos(DimensionalBlockPos pos) {
        NBTTagCompound tagCompound = writeBlockPos(pos);
        tagCompound.setInteger("dim", pos.getDimension());
        return tagCompound;
    }

    public static BlockPos readBlockPos(NBTTagCompound tagCompound) {
        if(!tagCompound.hasKey("x") || !tagCompound.hasKey("y") || !tagCompound.hasKey("z"))
            return new BlockPos(0, 0, 0);

        int x = tagCompound.getInteger("x");
        int y = tagCompound.getInteger("y");
        int z = tagCompound.getInteger("z");

        return new BlockPos(x, y, z);
    }

    public static DimensionalBlockPos readDimensionalBlockPos(NBTTagCompound tagCompound) {
        BlockPos pos = readBlockPos(tagCompound);

        int dim = tagCompound.getInteger("dim");

        return new DimensionalBlockPos(dim, pos);
    }
}
