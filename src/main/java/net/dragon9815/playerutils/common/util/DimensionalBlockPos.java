/*
 * This file is part of PlayerUtils
 * Copyright (C) 2016 Dragon9815 and others.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package net.dragon9815.playerutils.common.util;

import net.minecraft.entity.Entity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3i;

public class DimensionalBlockPos extends BlockPos {
    private final int dimension;

    public DimensionalBlockPos(int dim, int x, int y, int z) {
        super(x, y, z);
        this.dimension = dim;
    }

    public DimensionalBlockPos(int dim, double x, double y, double z) {
        super(x, y, z);
        this.dimension = dim;
    }

    public DimensionalBlockPos(Entity source) {
        super(source);
        this.dimension = source.dimension;
    }

    public DimensionalBlockPos(int dim, Vec3i source) {
        super(source);
        this.dimension = dim;
    }

    public int getDimension() {
        return dimension;
    }
}
