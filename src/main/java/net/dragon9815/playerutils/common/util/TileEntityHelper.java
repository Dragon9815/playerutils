/*
 * This file is part of PlayerUtils
 * Copyright (C) 2016 Dragon9815 and others.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package net.dragon9815.playerutils.common.util;

import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.common.DimensionManager;

public class TileEntityHelper {
    public static TileEntity getTileEntityInDimension(DimensionalBlockPos pos) {
        World world = DimensionManager.getWorld(pos.getDimension());

        if(world == null)
            return null;

        return world.getTileEntity(pos);
    }

    public static TileEntity getTileEntityInWorld(World world, BlockPos pos) {
        if(world == null)
            return null;

        return world.getTileEntity(pos);
    }
}
