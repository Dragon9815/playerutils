/*
 * This file is part of PlayerUtils
 * Copyright (C) 2016 Dragon9815 and others.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package net.dragon9815.playerutils.common.tileentity;

import net.dragon9815.playerutils.common.capabilities.player.CapabilityPlayerHandler;
import net.dragon9815.playerutils.common.capabilities.player.IPlayerHandler;
import net.dragon9815.playerutils.common.capabilities.upgrade.CapabilityUpgradeHandler;
import net.dragon9815.playerutils.common.capabilities.upgrade.IUpgradeHandler;
import net.dragon9815.playerutils.common.inventory.slot.SlotPlayer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SPacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.Constants;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class TileEntityBase extends TileEntity {
    private String customName;
    private int renderedFragment = 0;

    @SuppressWarnings("unchecked")
    private ArrayList<CapabilityHolder>[] sideCapabilities = new ArrayList[7];

    public TileEntityBase() {
        for(int i = 0; i < sideCapabilities.length; i++) {
            sideCapabilities[i] = new ArrayList<>();
        }
    }

    public void dropInventory() {
        if(hasCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, null)) {
            IItemHandler itemHandler = getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, null);

            for (int i = 0; i < itemHandler.getSlots(); i++) {
                ItemStack itemStack = itemHandler.getStackInSlot(i);
                dropItemStack(itemStack);
            }
        }

        if(hasCapability(CapabilityPlayerHandler.PLAYER_HANDLER_CAPABILITY, null)) {
            IPlayerHandler playerHandler = getCapability(CapabilityPlayerHandler.PLAYER_HANDLER_CAPABILITY, null);

            SlotPlayer slotPlayer = new SlotPlayer(playerHandler, 0, 0, 0);
            ItemStack stack = slotPlayer.getStack();
            dropItemStack(stack);
        }

        if(hasCapability(CapabilityUpgradeHandler.UPGRADE_HANDLER_CAPABILITY, null)) {
            IUpgradeHandler upgradeHandler = getCapability(CapabilityUpgradeHandler.UPGRADE_HANDLER_CAPABILITY, null);

            for(int i = 0; i < upgradeHandler.getNumUpgrades(); i++) {
                ItemStack stack = upgradeHandler.getUpgradeInSlot(i);
                dropItemStack(stack);
            }
        }
    }

    private void dropItemStack(ItemStack itemStack) {
        if (itemStack != null && itemStack.stackSize > 0) {
            Random rand = new Random();

            float dX = rand.nextFloat() * 0.8F + 0.1F;
            float dY = rand.nextFloat() * 0.8F + 0.1F;
            float dZ = rand.nextFloat() * 0.8F + 0.1F;

            EntityItem entityItem = new EntityItem(worldObj, pos.getX() + dX, pos.getY() + dY, pos.getZ() + dZ, itemStack.copy());

            if (itemStack.hasTagCompound()) {
                entityItem.getEntityItem().setTagCompound(itemStack.getTagCompound().copy());
            }

            float factor = 0.05F;
            entityItem.motionX = rand.nextGaussian() * factor;
            entityItem.motionY = rand.nextGaussian() * factor + 0.2F;
            entityItem.motionZ = rand.nextGaussian() * factor;
            worldObj.spawnEntityInWorld(entityItem);
            itemStack.stackSize = 0;
        }
    }

    @Override
    public SPacketUpdateTileEntity getUpdatePacket() {
        NBTTagCompound data = new NBTTagCompound();
        writeToNBT(data);
        return new SPacketUpdateTileEntity(pos, 1, data);
    }

    @Override
    public void onDataPacket(NetworkManager networkManager, SPacketUpdateTileEntity sPacketUpdateTileEntity) {
        readFromNBT(sPacketUpdateTileEntity.getNbtCompound());
        worldObj.markBlockRangeForRenderUpdate(pos, pos);
        markForUpdate();
    }

    public void markForUpdate() {
        if (this.renderedFragment > 0) {
            this.renderedFragment |= 0x1;
        }
        else if (this.worldObj != null) {
            markDirty();

            IBlockState state = worldObj.getBlockState(pos);
            worldObj.notifyNeighborsOfStateChange(pos, state.getBlock());

            worldObj.notifyBlockUpdate(this.pos, state, state, 3);
        }
    }

    @Override
    public void onChunkUnload() {
        if (!this.isInvalid())
            this.invalidate();
    }

    public String getName() {
        return hasCustomName() ? this.customName : getClass().getSimpleName();
    }

    public boolean hasCustomName() {
        return (this.customName != null) && (this.customName.length() > 0);
    }

    public void setName(String name) {
        this.customName = name;
    }

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound nbtTagCompound) {
        super.writeToNBT(nbtTagCompound);

        if (this.customName != null) {
            nbtTagCompound.setString("customName", this.customName);
        }

        NBTTagList tagList = new NBTTagList();
        for(int i = 0; i < sideCapabilities.length; i++) {
            List<CapabilityHolder> list = sideCapabilities[i];
            NBTTagCompound tagCompound = new NBTTagCompound();

            for(CapabilityHolder holder : list) {
                @SuppressWarnings("unchecked")
                NBTBase nbtBase = holder.capability.getStorage().writeNBT(holder.capability, holder.handler, (i < EnumFacing.values().length) ? EnumFacing.values()[i] : null);
                if(nbtBase == null)
                    nbtBase = new NBTTagCompound();
                tagCompound.setTag(holder.capability.getName(), nbtBase);
            }

            tagList.appendTag(tagCompound);
        }

        nbtTagCompound.setTag("CapabilityData", tagList);

        return nbtTagCompound;
    }

    @Override
    @SuppressWarnings("unchecked")
    public void readFromNBT(NBTTagCompound nbtTagCompound) {
        super.readFromNBT(nbtTagCompound);

        if (nbtTagCompound.hasKey("customName")) {
            this.customName = nbtTagCompound.getString("customName");
        }
        else {
            this.customName = null;
        }

        NBTTagList tagList = nbtTagCompound.getTagList("CapabilityData", Constants.NBT.TAG_COMPOUND);
        for(int i = 0; i < tagList.tagCount(); i++) {
            NBTTagCompound tagCompound = tagList.getCompoundTagAt(i);

            for (CapabilityHolder holder : sideCapabilities[i]) {
                if (tagCompound.hasKey(holder.capability.getName())) {
                    NBTBase nbtBase = tagCompound.getTag(holder.capability.getName());
                    holder.capability.getStorage().readNBT(holder.capability, holder.handler, (i < EnumFacing.values().length) ? EnumFacing.values()[i] : null, nbtBase);
                }
            }
        }
    }

    protected <T> void addCapability(EnumFacing facing, Capability<T> capability, T handler) {
        if(facing == null)
            sideCapabilities[EnumFacing.values().length].add(new CapabilityHolder<T>(capability, handler));
        else
            sideCapabilities[facing.ordinal()].add(new CapabilityHolder<T>(capability, handler));
    }

    protected <T> void addCapabilityAllSides(Capability<T> capability, T handler) {
        for(EnumFacing facing : EnumFacing.VALUES) {
            addCapability(facing, capability, handler);
        }
        addCapability(null, capability, handler);
    }

    @Override
    public boolean hasCapability(Capability<?> capability, @Nullable EnumFacing facing) {
        List<CapabilityHolder> capabilityHolders;
        if(facing == null) {
            capabilityHolders = sideCapabilities[EnumFacing.values().length];
        }
        else {
            capabilityHolders = sideCapabilities[facing.ordinal()];
        }

        for(CapabilityHolder cap : capabilityHolders) {
            if(cap.capability == capability) {
                return true;
            }
        }

        return super.hasCapability(capability, facing);
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T> T getCapability(Capability<T> capability, @Nullable EnumFacing facing) {
        List<CapabilityHolder> capabilityHolders;
        if(facing == null) {
            capabilityHolders = sideCapabilities[EnumFacing.values().length];
        }
        else {
            capabilityHolders = sideCapabilities[facing.ordinal()];
        }

        CapabilityHolder holder = null;

        for(CapabilityHolder cap : capabilityHolders) {
            if(cap.capability == capability) {
                holder = cap;
                break;
            }
        }

        if(holder == null)
            return super.getCapability(capability, facing);

        return (T)holder.handler;
    }

    private class CapabilityHolder<T> {
        public final T handler;
        @Nullable
        final Capability<T> capability;

        CapabilityHolder(@Nullable Capability<T> capability, T handler) {
            this.capability = capability;
            this.handler = handler;
        }
    }
}
