/*
 * This file is part of PlayerUtils
 * Copyright (C) 2016 Dragon9815 and others.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package net.dragon9815.playerutils.common.tileentity.machines;

import cofh.api.energy.EnergyStorage;
import cofh.api.energy.IEnergyReceiver;
import net.dragon9815.playerutils.common.capabilities.player.CapabilityPlayerHandler;
import net.dragon9815.playerutils.common.capabilities.player.PlayerHandler;
import net.dragon9815.playerutils.common.capabilities.upgrade.CapabilityUpgradeHandler;
import net.dragon9815.playerutils.common.capabilities.upgrade.UpgradeHandler;
import net.dragon9815.playerutils.common.helper.LogHelper;
import net.dragon9815.playerutils.common.reference.UpgradeEffects;
import net.dragon9815.playerutils.common.tileentity.TileEntityBase;
import net.dragon9815.playerutils.common.util.EnumUpgrade;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ITickable;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.ItemStackHandler;
import net.minecraftforge.items.wrapper.InvWrapper;

public class TileEntityItemTransporter extends TileEntityBase implements ITickable, IEnergyReceiver {

    private final UpgradeHandler upgradeHandler;
    private final ItemStackHandler itemHandler;
    private final PlayerHandler playerHandler;

    private EnergyStorage energyStorage;

    private int maxTransferSize = UpgradeEffects.Speed.SLOW;
    private int range = UpgradeEffects.Range.SMALL;
    private boolean multidim = false;

    private int energyPerItem = 50;

    public TileEntityItemTransporter() {
        super();

        upgradeHandler = new UpgradeHandler(EnumUpgrade.SPEED, EnumUpgrade.MULTIDIMENSIONAL, EnumUpgrade.RANGE);
        itemHandler = new ItemStackHandler(20);
        playerHandler = new PlayerHandler(this);

        addCapability(null, CapabilityUpgradeHandler.UPGRADE_HANDLER_CAPABILITY, upgradeHandler);
        addCapabilityAllSides(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, itemHandler);
        addCapability(null, CapabilityPlayerHandler.PLAYER_HANDLER_CAPABILITY, playerHandler);

        energyStorage = new EnergyStorage(10000);
    }

    @Override
    public void update() {
        if(!worldObj.isRemote) {
            if (playerHandler.getPlayerEntity() != null) {
                EntityPlayer player = playerHandler.getPlayerEntity();

                int distance = 0;
                if(!multidim && range != UpgradeEffects.Range.INFINITE) {
                    int dx = pos.getX() - (int) player.posX;
                    int dy = pos.getY() - (int) player.posY;
                    distance = (int) Math.sqrt(dx * dx + dy * dy);
                }

                if((multidim && range == UpgradeEffects.Range.INFINITE) || (player.worldObj.provider.getDimension() == worldObj.provider.getDimension() && (range == UpgradeEffects.Range.INFINITE || distance <= (range + 1)))) {
                    InvWrapper playerWrapper = new InvWrapper(player.inventory);
                    int maxTransferSize = this.maxTransferSize;

                    boolean extracted = false;

                    for (int i = 0; i < itemHandler.getSlots() && !extracted; i++) {
                        ItemStack stack = itemHandler.getStackInSlot(i);
                        if (stack != null) {
                            stack = stack.copy();

                            if (stack.stackSize > maxTransferSize)
                                stack.stackSize = maxTransferSize;

                            for (int j = 0; j < player.inventory.getSizeInventory() && stack != null; j++) {
                                ItemStack playerStack = player.inventory.getStackInSlot(j);
                                if (playerStack == null || (playerStack.isStackable() && playerStack.isItemEqual(stack) && playerStack.stackSize < playerStack.getMaxStackSize())) {
                                    // Get how much items can't be inserted in that specific slot
                                    ItemStack remainder = playerWrapper.insertItem(j, stack, true);

                                    // Set the stackSize to 0 instead of setting the stack being null
                                    if(remainder == null)
                                        remainder = new ItemStack(stack.getItem(), 0);

                                    int itemsToExtract = stack.stackSize - remainder.stackSize;

                                    // Calculate how much energy is needed
                                    int energyNeeded = itemsToExtract * energyPerItem;

                                    // Check if enough energy is available
                                    if (energyNeeded > energyStorage.getEnergyStored()) {
                                        // Energy not enough -> calculate how much items can be transported (must be lower than stackExtracted.stackSize)!
                                        itemsToExtract = energyStorage.getEnergyStored() / energyPerItem;
                                        // Recalculate the needed energy
                                        energyNeeded = itemsToExtract * energyPerItem;
                                    }

                                    // Extract stack out of the inventory of the tileentity
                                    ItemStack stackExtracted = itemHandler.extractItem(i, itemsToExtract, false);

                                    // Insert the stack into the inventory of the player
                                    ItemStack remainder1 = playerWrapper.insertItem(j, stackExtracted, false);

                                    stack.stackSize -= itemsToExtract;
                                    // Actually extract the energy
                                    energyStorage.extractEnergy(energyNeeded, false);

                                    extracted = true;

                                    if (stack.stackSize <= 0) {
                                        stack = null;
                                    }


                                }
                            }
                        }
                    }

                    markForUpdate();
                }
            }

            if(upgradeHandler.hasChanged()) {
                maxTransferSize = UpgradeEffects.Speed.levels[upgradeHandler.getUpgradeLevel(EnumUpgrade.SPEED)];
                range = UpgradeEffects.Range.levels[upgradeHandler.getUpgradeLevel(EnumUpgrade.RANGE)];
                multidim = upgradeHandler.getUpgradeLevel(EnumUpgrade.MULTIDIMENSIONAL) > 0;
            }
        }
    }

    @Override
    public boolean canConnectEnergy(EnumFacing from) {
        return true;
    }

    @Override
    public int getEnergyStored(EnumFacing from) {
        return energyStorage.getEnergyStored();
    }

    @Override
    public int getMaxEnergyStored(EnumFacing from) {
        return energyStorage.getMaxEnergyStored();
    }

    @Override
    public int receiveEnergy(EnumFacing from, int maxReceive, boolean simulate) {
        int receivedEnergy = energyStorage.receiveEnergy(maxReceive, simulate);

        if(receivedEnergy > 0 && !simulate)
            markForUpdate();

        return receivedEnergy;
    }

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound nbtTagCompound) {
        super.writeToNBT(nbtTagCompound);

        NBTTagCompound tagCompound = new NBTTagCompound();
        energyStorage.writeToNBT(tagCompound);
        nbtTagCompound.setTag("energy", tagCompound);

        return nbtTagCompound;
    }

    @Override
    public void readFromNBT(NBTTagCompound nbtTagCompound) {
        super.readFromNBT(nbtTagCompound);

        energyStorage.readFromNBT(nbtTagCompound.getCompoundTag("energy"));
    }
}
