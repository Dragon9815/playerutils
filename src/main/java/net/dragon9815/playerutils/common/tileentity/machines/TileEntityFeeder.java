/*
 * This file is part of PlayerUtils
 * Copyright (C) 2016 Dragon9815 and others.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package net.dragon9815.playerutils.common.tileentity.machines;

import cofh.api.energy.EnergyStorage;
import cofh.api.energy.IEnergyReceiver;
import net.dragon9815.playerutils.common.capabilities.player.CapabilityPlayerHandler;
import net.dragon9815.playerutils.common.capabilities.player.PlayerHandler;
import net.dragon9815.playerutils.common.capabilities.upgrade.CapabilityUpgradeHandler;
import net.dragon9815.playerutils.common.capabilities.upgrade.UpgradeHandler;
import net.dragon9815.playerutils.common.tileentity.TileEntityBase;
import net.dragon9815.playerutils.common.util.EnumUpgrade;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemFood;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ITickable;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.ItemStackHandler;

public class TileEntityFeeder extends TileEntityBase implements ITickable, IEnergyReceiver {
    private static final int energyPerHunger = 10;

    // Handlers
    private ItemStackHandler itemHandler;
    private UpgradeHandler upgradeHandler;
    private PlayerHandler playerHandler;

    private EnergyStorage energyStorage;

    public TileEntityFeeder() {
        super();

        itemHandler = new ItemStackHandler(9);
        upgradeHandler = new UpgradeHandler(EnumUpgrade.RANGE, EnumUpgrade.FOOD_EFFICIENCY, EnumUpgrade.MULTIDIMENSIONAL);
        playerHandler = new PlayerHandler(this);

        addCapabilityAllSides(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, itemHandler);
        addCapability(null, CapabilityUpgradeHandler.UPGRADE_HANDLER_CAPABILITY, upgradeHandler);
        addCapability(null, CapabilityPlayerHandler.PLAYER_HANDLER_CAPABILITY, playerHandler);

        energyStorage = new EnergyStorage(5000);
    }

    @Override
    public void update() {
        if (!worldObj.isRemote) {

            if(energyStorage.extractEnergy(20, true) >= 20) {
                energyStorage.extractEnergy(20, false);

                EntityPlayer player = playerHandler.getPlayerEntity();

                if (player != null) {
                    for (int i = 0; i < itemHandler.getSlots(); i++) {
                        ItemStack stack = itemHandler.extractItem(i, 1, true);
                        if (stack == null)
                            continue;

                        // No food, not interesting
                        if (!(stack.getItem() instanceof ItemFood))
                            continue;

                        int foodLevel = player.getFoodStats().getFoodLevel();
                        int fillLevel = ((ItemFood) stack.getItem()).getHealAmount(stack);

                        // Food full
                        if (foodLevel == 20.0F)
                            break;

                        // Only feed if the player really needs it, also feed when the food level gets to 6, so the player can sprint
                        if (foodLevel <= 6.0F || 20.0F - foodLevel <= fillLevel) {
                            int energyNeeded = fillLevel * energyPerHunger;

                            if(energyStorage.extractEnergy(energyNeeded, true) >= energyNeeded) {
                                energyStorage.extractEnergy(energyNeeded, false);
                                itemHandler.extractItem(i, 1, false);
                                player.getFoodStats().addStats((ItemFood) stack.getItem(), stack);
                            }
                        }
                    }
                }
            }
        }
    }

    @Override
    public boolean canConnectEnergy(EnumFacing from) {
        return true;
    }

    @Override
    public int getEnergyStored(EnumFacing from) {
        return energyStorage.getEnergyStored();
    }

    @Override
    public int getMaxEnergyStored(EnumFacing from) {
        return energyStorage.getMaxEnergyStored();
    }

    @Override
    public int receiveEnergy(EnumFacing from, int maxReceive, boolean simulate) {
        return energyStorage.receiveEnergy(maxReceive, simulate);
    }
}
