/*
 * This file is part of PlayerUtils
 * Copyright (C) 2016 Dragon9815 and others.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package net.dragon9815.playerutils.common.tileentity.machines;

import cofh.api.energy.EnergyStorage;
import cofh.api.energy.IEnergyReceiver;
import net.dragon9815.playerutils.common.capabilities.player.CapabilityPlayerHandler;
import net.dragon9815.playerutils.common.capabilities.player.PlayerHandler;
import net.dragon9815.playerutils.common.capabilities.upgrade.CapabilityUpgradeHandler;
import net.dragon9815.playerutils.common.capabilities.upgrade.UpgradeHandler;
import net.dragon9815.playerutils.common.inventory.WrappedInventory;
import net.dragon9815.playerutils.common.tileentity.TileEntityBase;
import net.dragon9815.playerutils.common.util.EnumUpgrade;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ITickable;

public class TileEntityComparatorBridge extends TileEntityBase implements ITickable, IEnergyReceiver {
    private PlayerHandler playerHandler;
    private UpgradeHandler upgradeHandler;

    private WrappedInventory prevInventory;
    private EnergyStorage energyStorage;

    public TileEntityComparatorBridge() {
        playerHandler = new PlayerHandler(this);
        upgradeHandler = new UpgradeHandler(EnumUpgrade.RANGE, EnumUpgrade.MULTIDIMENSIONAL);

        addCapability(null, CapabilityPlayerHandler.PLAYER_HANDLER_CAPABILITY, playerHandler);
        addCapability(null, CapabilityUpgradeHandler.UPGRADE_HANDLER_CAPABILITY, upgradeHandler);

        energyStorage = new EnergyStorage(5000);
    }

    @Override
    public void update() {
        if (!worldObj.isRemote) {
            if (energyStorage.getEnergyStored() >= 20) {
                energyStorage.extractEnergy(20, false);

                EntityPlayer player = playerHandler.getPlayerEntity();

                if(player != null) {
                    if (prevInventory == null)
                        prevInventory = WrappedInventory.fromIInventory(player.inventory);
                    else {
                        if (!prevInventory.compare(WrappedInventory.fromIInventory(player.inventory))) {
                            prevInventory = WrappedInventory.fromIInventory(player.inventory);
                        }
                    }
                }

                markForUpdate();
            }
        }
    }

    public boolean canOutputToComparator() {
        return playerHandler.getPlayerEntity() != null && energyStorage.getEnergyStored() >= 20;
    }

    @Override
    public boolean canConnectEnergy(EnumFacing from) {
        return true;
    }

    @Override
    public int getEnergyStored(EnumFacing from) {
        return energyStorage.getEnergyStored();
    }

    @Override
    public int getMaxEnergyStored(EnumFacing from) {
        return energyStorage.getMaxEnergyStored();
    }

    @Override
    public int receiveEnergy(EnumFacing from, int maxReceive, boolean simulate) {
        return energyStorage.receiveEnergy(maxReceive, simulate);
    }
}
