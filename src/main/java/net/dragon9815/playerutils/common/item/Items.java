/*
 * This file is part of PlayerUtils
 * Copyright (C) 2016 Dragon9815 and others.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package net.dragon9815.playerutils.common.item;

import net.dragon9815.playerutils.common.item.tools.ItemDebug;
import net.dragon9815.playerutils.common.item.tools.ItemPlayerCard;
import net.dragon9815.playerutils.common.util.IProvideRecipe;
import net.dragon9815.playerutils.common.util.Platform;
import net.minecraft.item.ItemStack;

public enum Items {
    DEBUG(ItemDebug.class),
    PLAYER_CARD(ItemPlayerCard.class),
    UPGRADE(ItemUpgrade.class)
    ;

    private static boolean registered = false;
    private static boolean registeredRecipes = false;
    private static boolean registeredRenderers = false;

    private final Class<? extends ItemBase> itemClass;
    private ItemBase item;

    Items(Class<? extends ItemBase> itemClass) {
        this.itemClass = itemClass;
    }

    private void register() {
        item = Platform.registerItem(itemClass);
    }

    public ItemBase getItem() {
        return item;
    }

    public ItemStack getStack(int damage, int size) {
        return new ItemStack(item, size, damage);
    }

    public ItemStack getStack(int size) {
        return new ItemStack(item, size, 0);
    }

    public static void registerAll() {
        if (registered)
            return;

        for (Items i : Items.values())
            i.register();

        registered = true;
    }

    public static void registerRecipes() {
        if (registeredRecipes)
            return;

        for (Items i : Items.values()) {
            ItemBase item = i.getItem();
            if(item instanceof IProvideRecipe)
                ((IProvideRecipe)item).registerRecipes();
        }

        registeredRecipes = true;
    }

    public static void registerRenderers() {
        if(registeredRenderers)
            return;

        for (Items i : Items.values()) {
            i.getItem().registerItemRenderers();
        }

        registeredRenderers = true;
    }
}
