package net.dragon9815.playerutils.common.item.tools;

import net.dragon9815.playerutils.common.creativetab.CreativeTabsPlayerUtils;
import net.dragon9815.playerutils.common.helper.StringHelper;
import net.dragon9815.playerutils.common.item.ItemBase;
import net.dragon9815.playerutils.common.reference.Reference;
import net.dragon9815.playerutils.common.util.PlayerData;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.ItemMeshDefinition;
import net.minecraft.client.renderer.block.model.ModelBakery;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.EnumAction;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.*;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.world.World;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import java.util.List;

public class ItemPlayerCard extends ItemBase {
    public static final ModelResourceLocation UNBOUND_LOCATION = new ModelResourceLocation(new ResourceLocation(Reference.MOD_ID, "tools/player_card_unbound"), "inventory");
    public static final ModelResourceLocation BOUND_LOCATION = new ModelResourceLocation(new ResourceLocation(Reference.MOD_ID, "tools/player_card_bound"), "inventory");

    public ItemPlayerCard() {
        super("", "player_card", CreativeTabsPlayerUtils.tabMachines);

        setMaxStackSize(1);
        setMaxDamage(0);
    }

    @Override
    public void addInformation(ItemStack stack, EntityPlayer playerIn, List<String> tooltip, boolean advanced) {
        if(stack.hasTagCompound()) {
            PlayerData data = PlayerData.readFromToNBT(stack.getTagCompound());

            if(data.isValid()) {
                tooltip.add("Bound to: " + data.name);
                if (advanced) {
                    tooltip.add("UUID: " + data.uuid);
                }
            }
        }
        else {
            tooltip.add(StringHelper.translateToLocal("desc.playerutils.player_unbound"));
        }
    }

    @Override
    public ActionResult<ItemStack> onItemRightClick(ItemStack stack, World worldIn, EntityPlayer playerIn, EnumHand hand) {
        return new ActionResult<>(rightClick(stack, playerIn, worldIn), stack);
    }

    @Override
    public EnumActionResult onItemUse(ItemStack stack, EntityPlayer playerIn, World worldIn, BlockPos pos, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ) {
        return rightClick(stack, playerIn, worldIn);
    }

    private EnumActionResult rightClick(ItemStack stack, EntityPlayer player, World world) {
        if (player.isSneaking() && stack.hasTagCompound()) {
            stack.setTagCompound(null);
            stack.setItemDamage(0);

            return EnumActionResult.SUCCESS;
        }
        else if(!stack.hasTagCompound() && !player.isSneaking()){
            PlayerData data = PlayerData.fromPlayer(player);

            if(data.isValid()) {
                stack.setTagCompound(data.writeToNBT(new NBTTagCompound()));
                stack.setItemDamage(1);
                world.playSound(null, player.posX, player.posY, player.posZ, SoundEvents.ENTITY_EXPERIENCE_ORB_PICKUP, SoundCategory.NEUTRAL, 0.5f, 1.0f);
            }

            return EnumActionResult.SUCCESS;
        }

        return EnumActionResult.PASS;
    }

    @Override
    @SideOnly(Side.CLIENT)
    public void registerItemRenderers() {
        ModelBakery.registerItemVariants(this, UNBOUND_LOCATION, BOUND_LOCATION);
        ModelLoader.setCustomMeshDefinition(this, new MeshDefinition());
    }

    private static class MeshDefinition implements ItemMeshDefinition {
        @Override
        public ModelResourceLocation getModelLocation(ItemStack stack) {
            if(stack.getItemDamage() == 0)
                return UNBOUND_LOCATION;
            else if(stack.getItemDamage() == 1)
                return BOUND_LOCATION;

            return UNBOUND_LOCATION;
        }
    }
}
