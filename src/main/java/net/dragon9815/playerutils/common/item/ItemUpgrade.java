/*
 * This file is part of PlayerUtils
 * Copyright (C) 2016 Dragon9815 and others.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package net.dragon9815.playerutils.common.item;

import net.dragon9815.playerutils.common.creativetab.CreativeTabsPlayerUtils;
import net.dragon9815.playerutils.common.item.ItemBase;
import net.dragon9815.playerutils.common.reference.Reference;
import net.dragon9815.playerutils.common.util.EnumUpgrade;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.client.model.ModelLoader;

import java.util.List;

public class ItemUpgrade extends ItemBase {

    public ItemUpgrade() {
        super("upgrade", "upgrade", CreativeTabsPlayerUtils.tabMachines);

        setHasSubtypes(true);
        setMaxStackSize(8);
    }

    @Override
    public int getMetadata(int damage) {
        return damage;
    }

    @Override
    public String getUnlocalizedName(ItemStack stack) {
        String s = super.getUnlocalizedName(stack);

        return String.format("%s_%s", super.getUnlocalizedName(stack), EnumUpgrade.byMeta(stack.getMetadata()).getName());
    }

    @Override
    public void getSubItems(Item itemIn, CreativeTabs tab, List<ItemStack> subItems) {
        for(int i = 0; i < EnumUpgrade.values().length; i++) {
            subItems.add(new ItemStack(this, 1, i));
        }
    }

    @Override
    public void registerItemRenderers() {
        for (int i = 0; i < EnumUpgrade.values().length; i++) {
            String path = String.format("%s:%s_%s", Reference.MOD_ID, resourcePath, EnumUpgrade.byMeta(i).getName());

            ModelLoader.setCustomModelResourceLocation(this, i, new ModelResourceLocation(path, "inventory"));
        }
    }
}
