/*
 * This file is part of PlayerUtils
 * Copyright (C) 2016 Dragon9815 and others.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package net.dragon9815.playerutils.common.item.tools;

import net.dragon9815.playerutils.common.creativetab.CreativeTabsPlayerUtils;
import net.dragon9815.playerutils.common.item.ItemBase;
import net.minecraft.block.Block;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.world.World;

public class ItemDebug extends ItemBase {

    public ItemDebug() {
        super("tools/debug", "debug", CreativeTabsPlayerUtils.tabMachines);
    }

    @Override
    public EnumActionResult onItemUse(ItemStack stack, EntityPlayer playerIn, World worldIn, BlockPos pos, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ) {

        //NetworkHandler.INSTANCE.sendToServer(new MessagePrintInfo(pos.getX(), pos.getY(), pos.getZ(), worldIn.provider.getDimensionId(), playerIn.getUniqueID()));
        if (!worldIn.isRemote) {
            Block block = worldIn.getBlockState(pos).getBlock();

            playerIn.addChatMessage(new TextComponentString(block.getLocalizedName() + ":"));

            TileEntity te = worldIn.getTileEntity(pos);

            if (te == null)
                return EnumActionResult.PASS;

            if (te instanceof IInventory) {
                IInventory inv = (IInventory) te;

                for (int i = 0; i < inv.getSizeInventory(); i++) {
                    playerIn.addChatMessage(new TextComponentString(String.valueOf(inv.getStackInSlot(i))));
                }
            }
        }

        return EnumActionResult.SUCCESS;
    }
}
