/*
 * This file is part of PlayerUtils
 * Copyright (C) 2016 Dragon9815 and others.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package net.dragon9815.playerutils.common.item;

import net.dragon9815.playerutils.common.helper.StringHelper;
import net.dragon9815.playerutils.common.reference.Reference;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.client.model.ModelLoaderRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public abstract class ItemBase extends Item {

    private String internalName;
    protected String resourcePath;

    protected ItemBase(String resourcePath, String internalName, CreativeTabs creativeTab) {
        this.resourcePath = resourcePath;
        this.internalName = internalName;

        setCreativeTab(creativeTab);
    }


    public String getResourcePath() {
        return resourcePath;
    }

    @Override
    public String getUnlocalizedName() {
        String itemName = getUnwrappedUnlocalizedName(super.getUnlocalizedName());

        String s = String.format("item.%s:%s", Reference.MOD_ID, itemName);
        return s;
    }

    @Override
    public String getUnlocalizedName(ItemStack stack) {
        String itemName = getUnwrappedUnlocalizedName(super.getUnlocalizedName(stack));

        String s = String.format("item.%s:%s", Reference.MOD_ID, itemName);
        return s;
    }

    protected String getUnwrappedUnlocalizedName(String unlocalizedName) {
        return unlocalizedName.substring(unlocalizedName.indexOf('.') + 1);
    }

    public String getInternalName() {
        return internalName;
    }

    public String getLocalizedName() {
        return StringHelper.translateToLocal(String.format("%s.name", getUnlocalizedName()));
    }

    @SideOnly(Side.CLIENT)
    public void registerItemRenderers() {
        String path = String.format("%s:%s", Reference.MOD_ID,resourcePath);

        ModelLoader.setCustomModelResourceLocation(this, 0, new ModelResourceLocation(path, "inventory"));
    }
}
