/*
 * This file is part of PlayerUtils
 * Copyright (C) 2016 Dragon9815 and others.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package net.dragon9815.playerutils.common.capabilities.upgrade;

import net.dragon9815.playerutils.common.util.EnumUpgrade;
import net.minecraft.item.ItemStack;

public class UpgradeHandler implements IUpgradeHandlerModifyable {

    private Upgrade[] upgrades;
    private EnumUpgrade[] validUpgrades;

    private boolean changed;

    public UpgradeHandler(EnumUpgrade... validUpgrades) {
        this.validUpgrades = validUpgrades;
        this.upgrades = new Upgrade[validUpgrades.length];
    }

    @Override
    public int getNumUpgrades() {
        return upgrades.length;
    }

    @Override
    public ItemStack getUpgradeInSlot(int slot) {
        if(slot >= upgrades.length || upgrades[slot] == null)
            return null;

        return upgrades[slot].toItemStack();
    }

    @Override
    public void clearSlot(int slot) {
        setUpgradeInSlot(slot, null);
    }

    @Override
    public ItemStack insertUpgrade(int slot, ItemStack upgradeStack, boolean simulate) {
        if(slot >= upgrades.length)
            return upgradeStack;

        Upgrade upgrade = Upgrade.fromItemStack(upgradeStack);

        if(upgrade == null)
            return upgradeStack;

        if(!isUpgradeValid(upgrade.type))
            return upgradeStack;

        // Get the number of upgrades of the same type in the other slots
        int numUpgrades = getUpgradeLevel(upgrade.type);

        int inserted = 0;

        if(upgrades[slot] != null) {
            if(!upgrades[slot].equalsType(upgrade.type))
                return upgradeStack;

            int maxInsert = upgrade.type.getMaxLevel() - numUpgrades;

            if(maxInsert > 0) {
                if (upgrade.level + numUpgrades > maxInsert) {
                    upgrade.level = maxInsert;
                }

                inserted = upgrade.level;

                if (!simulate) {
                    upgrades[slot].level += upgrade.level;
                    changed = true;
                }
            }
        }
        else {
            int maxInsert = upgrade.type.getMaxLevel() - numUpgrades;

            if(maxInsert > 0) {
                if (upgrade.level > maxInsert) {
                    upgrade.level = maxInsert;
                }

                inserted = upgrade.level;

                if (!simulate)
                    setUpgradeInSlot(slot, upgrade);
            }
        }

        upgradeStack.stackSize -= inserted;

        if (upgradeStack.stackSize <= 0) {
            upgradeStack = null;
        }

        return upgradeStack;
    }

    @Override
    public ItemStack extractUpgrade(int slot, int maxExtract, boolean simulate) {
        if(slot >= upgrades.length)
            return null;

        if(upgrades[slot] == null)
            return null;

        ItemStack extracted = upgrades[slot].toItemStack();

        if(extracted.stackSize > maxExtract)
            extracted.stackSize = maxExtract;

        upgrades[slot].level -= extracted.stackSize;

        if(upgrades[slot].level <= 0)
            setUpgradeInSlot(slot, null);

        return extracted;
    }

    @Override
    public int getUpgradeLevel(EnumUpgrade upgradeType) {
        for(Upgrade upgrade : upgrades) {
            if(upgrade != null && upgrade.type.equals(upgradeType))
                return upgrade.level;
        }

        for(EnumUpgrade upgrade : validUpgrades) {
            if(upgrade.equals(upgradeType))
                return 0;
        }

        return -1;
    }

    @Override
    public boolean hasUpgrade(EnumUpgrade upgrade) {
        return getUpgradeLevel(upgrade) > 0;
    }

    private boolean isUpgradeValid(EnumUpgrade upgrade) {
        for(EnumUpgrade valid : validUpgrades) {
            if(upgrade.equals(valid))
                return true;
        }

        return false;
    }

    @Override
    public void setUpgradeInSlot(int slot, Upgrade upgrade) {
        if(slot < upgrades.length) {
            upgrades[slot] = upgrade;
            changed = true;
        }
    }

    public boolean hasChanged() {
        if(changed) {
            changed = false;
            return true;
        }

        return false;
    }
}
