package net.dragon9815.playerutils.common.capabilities.upgrade;

import net.dragon9815.playerutils.common.item.Items;
import net.dragon9815.playerutils.common.util.EnumUpgrade;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.common.util.INBTSerializable;

import javax.annotation.Nonnull;

public class Upgrade {
    public final EnumUpgrade type;
    public int level;

    public Upgrade(@Nonnull EnumUpgrade type, int level) {
        this.type = type;
        this.level = level;
    }

    @Override
    public boolean equals(Object obj) {
        if(!(obj instanceof Upgrade))
            return false;

        Upgrade u = ((Upgrade)obj);
        return u.type.equals(type) && u.level == level;
    }

    public boolean equalsType(EnumUpgrade u) {
        return u.equals(type);
    }

    public ItemStack toItemStack() {
        return new ItemStack(Items.UPGRADE.getItem(), level, type.getMeta());
    }

    public static Upgrade fromItemStack(ItemStack stack) {
        if(stack != null && stack.getItem().equals(Items.UPGRADE.getItem())) {
            return new Upgrade(EnumUpgrade.byMeta(stack.getMetadata()), stack.stackSize);
        }

        return null;
    }

    public NBTTagCompound writeToNBT(NBTTagCompound tagCompound) {
        tagCompound.setInteger("type", type.getMeta());
        tagCompound.setInteger("level", level);

        return tagCompound;
    }

    public static Upgrade readFromNBT(NBTTagCompound tagCompound) {
        return new Upgrade(EnumUpgrade.byMeta(tagCompound.getInteger("type")), tagCompound.getInteger("level"));
    }
}
