/*
 * This file is part of PlayerUtils
 * Copyright (C) 2016 Dragon9815 and others.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package net.dragon9815.playerutils.common.capabilities.player;

import net.dragon9815.playerutils.common.util.PlayerData;
import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityInject;
import net.minecraftforge.common.capabilities.CapabilityManager;

import java.util.concurrent.Callable;

public class CapabilityPlayerHandler {
    @CapabilityInject(IPlayerHandler.class)
    public static Capability<IPlayerHandler> PLAYER_HANDLER_CAPABILITY = null;
    private static boolean enabled = false;

    public static void enable() {
        if(!enabled) {
            enabled = true;

            CapabilityManager.INSTANCE.register(IPlayerHandler.class, new Capability.IStorage<IPlayerHandler>() {
                @Override
                public NBTBase writeNBT(Capability<IPlayerHandler> capability, IPlayerHandler instance, EnumFacing side) {
                    NBTTagCompound tagCompound = new NBTTagCompound();

                    if(instance != null && instance.getPlayer() != null) {
                        instance.getPlayer().writeToNBT(tagCompound);
                    }

                    return tagCompound;
                }

                @Override
                public void readNBT(Capability<IPlayerHandler> capability, IPlayerHandler instance, EnumFacing side, NBTBase nbt) {
                    if(instance != null && nbt instanceof NBTTagCompound) {
                        instance.setPlayer(PlayerData.readFromToNBT((NBTTagCompound)nbt));
                    }
                }
            }, new Callable<IPlayerHandler>() {
                @Override
                public IPlayerHandler call() throws Exception {
                    return new PlayerHandler(null);
                }
            });

        }
    }
}
