package net.dragon9815.playerutils.common.capabilities.upgrade;

import net.minecraft.item.ItemStack;

public interface IUpgradeHandlerModifyable extends IUpgradeHandler {
    void setUpgradeInSlot(int slot, Upgrade upgrade);
}
