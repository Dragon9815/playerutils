/*
 * This file is part of PlayerUtils
 * Copyright (C) 2016 Dragon9815 and others.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package net.dragon9815.playerutils.common.capabilities.upgrade;

import net.dragon9815.playerutils.common.util.EnumUpgrade;
import net.minecraft.item.ItemStack;

import javax.annotation.Nonnull;

/**
 * The interface for the type handler capability
 *
 * @author Dragon9815
 */
public interface IUpgradeHandler {
    /**
     * Returns the number of type slots in this handler.
     *
     * @return Number of type slots
     */
    int getNumUpgrades();

    /**
     * Returns the type in a specific slot.
     * If the index exceeds the bounds, null is returned
     *
     * @param slot The index of the slot to get
     * @return The ItemStack of the type in the specific slot
     */
    ItemStack getUpgradeInSlot(int slot);

    /**
     * Clears the slot.
     * @param slot The index of the slot
     */
    void clearSlot(int slot);

    /**
     * Inserts an ItemStack into the handler.
     *
     * @param slot The index of the slot
     * @param upgradeStack The ItemStack to insert
     * @param simulate If true, the handler won't be changed
     * @return The remainder of the ItemStack to insert
     */
    ItemStack insertUpgrade(int slot, @Nonnull ItemStack upgradeStack, boolean simulate);

    /**
     * Extracts an ItemStack from the handler up to a specific amount.
     *
     * @param slot
     * @param maxExtract
     * @param simulate
     * @return
     */
    ItemStack extractUpgrade(int slot, int maxExtract, boolean simulate);

    int getUpgradeLevel(EnumUpgrade upgrade);
    boolean hasUpgrade(EnumUpgrade upgrade);
}
