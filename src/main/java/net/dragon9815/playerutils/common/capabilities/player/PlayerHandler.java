/*
 * This file is part of PlayerUtils
 * Copyright (C) 2016 Dragon9815 and others.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package net.dragon9815.playerutils.common.capabilities.player;

import net.dragon9815.playerutils.common.helper.PlayerHelper;
import net.dragon9815.playerutils.common.tileentity.TileEntityBase;
import net.dragon9815.playerutils.common.util.PlayerData;
import net.minecraft.entity.player.EntityPlayer;

public class PlayerHandler implements IPlayerHandler {
    private PlayerData playerData = null;
    private TileEntityBase parent;

    public PlayerHandler(TileEntityBase parent) {
        this.parent = parent;
    }

    @Override
    public void setPlayer(PlayerData player) {
        playerData = player;
        markDirty();
    }

    @Override
    public PlayerData getPlayer() {
        return playerData;
    }

    @Override
    public EntityPlayer getPlayerEntity() {
        if(playerData == null)
            return null;

        return PlayerHelper.INSTANCE.getPlayer(playerData.uuid);
    }

    @Override
    public void markDirty() {
        if(parent != null)
            parent.markForUpdate();
    }
}
