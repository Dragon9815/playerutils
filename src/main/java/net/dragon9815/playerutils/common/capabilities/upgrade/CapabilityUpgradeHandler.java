/*
 * This file is part of PlayerUtils
 * Copyright (C) 2016 Dragon9815 and others.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package net.dragon9815.playerutils.common.capabilities.upgrade;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.util.EnumFacing;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityInject;
import net.minecraftforge.common.capabilities.CapabilityManager;

import java.util.concurrent.Callable;

public class CapabilityUpgradeHandler {
    @CapabilityInject(IUpgradeHandler.class)
    public static Capability<IUpgradeHandler> UPGRADE_HANDLER_CAPABILITY = null;
    private static boolean enabled = false;

    public static void enable() {
        if(!enabled) {
            enabled = true;
            CapabilityManager.INSTANCE.register(IUpgradeHandler.class, new Capability.IStorage<IUpgradeHandler>() {
                @Override
                public NBTBase writeNBT(Capability<IUpgradeHandler> capability, IUpgradeHandler instance, EnumFacing side) {
                    NBTTagList nbtTagList = new NBTTagList();
                    int size = instance.getNumUpgrades();
                    for (int i = 0; i < size; i++) {
                        ItemStack stack = instance.getUpgradeInSlot(i);
                        if (stack != null) {
                            NBTTagCompound itemTag = new NBTTagCompound();
                            itemTag.setInteger("slot", i);
                            nbtTagList.appendTag(Upgrade.fromItemStack(stack).writeToNBT(itemTag));
                        }
                    }
                    return nbtTagList;
                }

                @Override
                public void readNBT(Capability<IUpgradeHandler> capability, IUpgradeHandler instance, EnumFacing side, NBTBase base) {
                    if(instance instanceof IUpgradeHandlerModifyable) {
                        NBTTagList tagList = (NBTTagList) base;
                        for (int i = 0; i < tagList.tagCount(); i++) {
                            NBTTagCompound upgradeTag = tagList.getCompoundTagAt(i);
                            int j = upgradeTag.getInteger("slot");

                            if (j >= 0 && j < instance.getNumUpgrades()) {
                                ((IUpgradeHandlerModifyable)instance).setUpgradeInSlot(j, Upgrade.readFromNBT(upgradeTag));
                            }
                        }
                    }
                }
            }, new Callable<UpgradeHandler>() {
                @Override
                public UpgradeHandler call() throws Exception {
                    return new UpgradeHandler();
                }
            });
        }
    }
}
