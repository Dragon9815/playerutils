/*
 * This file is part of PlayerUtils
 * Copyright (C) 2016 Dragon9815 and others.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package net.dragon9815.playerutils.common.helper;

import net.dragon9815.playerutils.common.reference.Reference;
import net.minecraftforge.fml.common.FMLLog;
import org.apache.logging.log4j.Level;

public class LogHelper {

    public static void log(Object msg, Level level) {
        FMLLog.log(Reference.MOD_NAME, level, String.valueOf(msg));
    }

    public static void info(Object msg) {
        log(msg, Level.INFO);
    }

    public static void warn(Object msg) {
        log(msg, Level.WARN);
    }

    public static void fatal(Object msg) {
        log(msg, Level.FATAL);
    }

    public static void debug(Object msg) {
        log(msg, Level.DEBUG);
    }

    public static void trace(Object msg) {
        log(msg, Level.TRACE);
    }

    public static void all(Object msg) {
        log(msg, Level.ALL);
    }
}
