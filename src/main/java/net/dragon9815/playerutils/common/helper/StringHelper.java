package net.dragon9815.playerutils.common.helper;

import net.minecraft.util.text.TextComponentTranslation;

public class StringHelper {
    public static String translateToLocal(String key) {
        return new TextComponentTranslation(key).getUnformattedText();
    }
}
