/*
 * This file is part of PlayerUtils
 * Copyright (C) 2016 Dragon9815 and others.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package net.dragon9815.playerutils.common.helper;

import net.dragon9815.playerutils.common.util.PlayerData;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.fml.common.FMLCommonHandler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public class PlayerHelper {
    public static final PlayerHelper INSTANCE = new PlayerHelper();

    private ArrayList<PlayerData> allPlayers = new ArrayList<PlayerData>();
    private HashMap<UUID, EntityPlayer> playerEntityCache = new HashMap<UUID, EntityPlayer>();

    private PlayerHelper() {

    }

    public void clearCache() {
        playerEntityCache.clear();
    }

    public void setPlayerOnline(PlayerData player) {
        if(!isPlayerInList(allPlayers, player))
            allPlayers.add(player);

        PlayerData data = getPlayerFromList(allPlayers, player);

        if(data != null) {
            data.isOnline = true;
            playerEntityCache.put(data.uuid, PlayerHelper.INSTANCE.getPlayer(data.uuid));
        }
    }

    public void setPlayerOffline(PlayerData player) {
        PlayerData data = getPlayerFromList(allPlayers, player);

        if(data != null) {
            data.isOnline = false;
            playerEntityCache.remove(data);
        }
    }

    private boolean isPlayerInList(List<PlayerData> list, PlayerData player) {
        for(PlayerData p : list) {
            if(p.uuid.equals(player.uuid))
                return true;
        }

        return false;
    }

    private void deletePlayerFromList(List<PlayerData> list, PlayerData player) {
        for(int i = 0; i < list.size(); ) {
            PlayerData p = list.get(i);
            if(p.uuid.equals(player.uuid))
                list.remove(i);
            else
                i++;
        }
    }

    private PlayerData getPlayerFromList(List<PlayerData> list, PlayerData player) {
        for(PlayerData p : list) {
            if(p.uuid.equals(player.uuid))
                return p;
        }

        return null;
    }

    public boolean isPlayerOnline(PlayerData player) {
        PlayerData data = getPlayerFromList(allPlayers, player);

        return data != null && data.isOnline;
    }

    public EntityPlayer getPlayer(UUID uuid) {
        if(playerEntityCache.containsKey(uuid))
            return playerEntityCache.get(uuid);

        for (EntityPlayer player : FMLCommonHandler.instance().getMinecraftServerInstance().getPlayerList().getPlayerList()) {
            if (player.getUniqueID().equals(uuid)) {
                return player;
            }
        }

        return null;
    }

    public void load(ArrayList<PlayerData> data) {
        this.allPlayers = data;
    }

    public ArrayList<PlayerData> getAllPlayers() {
        return allPlayers;
    }
}
