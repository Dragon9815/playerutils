package net.dragon9815.playerutils.common.block;

import net.dragon9815.playerutils.common.tileentity.TileEntityBase;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.items.IItemHandler;

import java.util.Random;

public class BlockTile extends BlockBase implements ITileEntityProvider {

    private final Class<? extends TileEntity> tileClass;

    public BlockTile(String resourcePath, String internalName, CreativeTabs creativeTab, Class<? extends TileEntity> tileEntityClass) {
        super(resourcePath, internalName, creativeTab);
        this.tileClass = tileEntityClass;
        GameRegistry.registerTileEntity(tileEntityClass, internalName);
    }

    @Override
    public TileEntity createNewTileEntity(World world, int meta) {
        try {
            return this.tileClass.newInstance();
        } catch (Throwable e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void breakBlock(World world, BlockPos pos, IBlockState state) {
        TileEntity tileEntity = world.getTileEntity(pos);
        if(tileEntity instanceof TileEntityBase) {
            ((TileEntityBase)tileEntity).dropInventory();
        }

        super.breakBlock(world, pos, state);
    }
}
