/*
 * This file is part of PlayerUtils
 * Copyright (C) 2016 Dragon9815 and others.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package net.dragon9815.playerutils.common.block;

import net.dragon9815.playerutils.common.block.machine.BlockComparatorBridge;
import net.dragon9815.playerutils.common.block.machine.BlockFeeder;
import net.dragon9815.playerutils.common.block.machine.BlockItemTransporter;
import net.dragon9815.playerutils.common.util.IProvideRecipe;
import net.dragon9815.playerutils.common.util.Platform;
import net.minecraft.item.ItemBlock;

public enum Blocks {
    FEEDER(BlockFeeder.class),
    ITEM_TRANSPORTER(BlockItemTransporter.class),
    COMPARATOR_BRIDGE(BlockComparatorBridge.class),
    ;

    private static boolean registered = false;
    private static boolean registeredRecipes = false;
    private static boolean registeredRenderers = false;

    private BlockBase block;
    private final Class<? extends BlockBase> blockClass;
    private final Class<? extends ItemBlock> itemBlockClass;

    Blocks(Class<? extends BlockBase> blockClass, Class<? extends ItemBlock> itemBlockClass) {
        this.blockClass = blockClass;
        this.itemBlockClass = itemBlockClass;
    }

    Blocks(Class<?extends BlockBase> blockClass) {
        this(blockClass, ItemBlock.class);
    }

    private void registerBlock() {
        block = Platform.registerBlock(blockClass, itemBlockClass);
    }

    public BlockBase getBlock() {
        return block;
    }

    public static void registerAll() {
        if(registered)
            return;

        for (Blocks b : Blocks.values())
            b.registerBlock();

        registered = true;
    }

    public static void registerRecipes() {
        if(registeredRecipes)
            return;

        for(Blocks b : Blocks.values()) {
            BlockBase block = b.getBlock();
            if(block instanceof IProvideRecipe)
                ((IProvideRecipe)block).registerRecipes();
        }

        registeredRecipes = true;
    }

    public static void registerRenderers() {
        if(registeredRenderers)
            return;

        for(Blocks b : Blocks.values()) {
            BlockBase block = b.getBlock();
            block.registerBlockRenderers();
            block.registerBlockItemRenderers();
        }

        registeredRenderers = true;
    }
}
