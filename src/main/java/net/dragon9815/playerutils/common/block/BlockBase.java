/*
 * This file is part of PlayerUtils
 * Copyright (C) 2016 Dragon9815 and others.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package net.dragon9815.playerutils.common.block;

import net.dragon9815.playerutils.common.helper.StringHelper;
import net.dragon9815.playerutils.common.reference.Reference;
import net.dragon9815.playerutils.common.util.Platform;
import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.client.renderer.block.statemap.DefaultStateMapper;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumBlockRenderType;
import net.minecraftforge.client.model.ModelLoader;

import java.util.ArrayList;

public class BlockBase extends Block {

    private String resourcePath;
    private String internalName;

    protected BlockBase(Material mat, String resourcePath, String internalName, CreativeTabs creativeTab) {
        super(mat);

        this.resourcePath = resourcePath;
        this.internalName = internalName;

        setCreativeTab(creativeTab);

        setSoundType(SoundType.STONE);
        setHardness(2.2F);
        setResistance(5.0F);
        setHarvestLevel("pickaxe", 0);
        setLightOpacity(15);
    }

    protected BlockBase(String resourcePath, String internalName, CreativeTabs creativeTab) {
        this(Material.IRON, resourcePath, internalName, creativeTab);
    }

    @Override
    @SuppressWarnings("deprecation")
    public EnumBlockRenderType getRenderType(IBlockState state) {
        return EnumBlockRenderType.MODEL;
    }

    public String getInternalName() {
        return internalName;
    }

    public void registerBlockRenderers() {
        final String path = String.format("%s:%s", Reference.MOD_ID, resourcePath);

        ModelLoader.setCustomStateMapper(this, new DefaultStateMapper() {
            @Override
            protected ModelResourceLocation getModelResourceLocation(IBlockState state) {
                return new ModelResourceLocation(path, Platform.buildPropertyString(state.getProperties()));
            }
        });
    }

    public void registerBlockItemRenderers() {
        final String path = String.format("%s:%s", Reference.MOD_ID, resourcePath);

        ArrayList<ItemStack> blocks = new ArrayList<>();
        getSubBlocks(Item.getItemFromBlock(this), null, blocks);

        for(ItemStack stack : blocks) {
            IBlockState state = this.getStateFromMeta(stack.getItemDamage());
            ModelLoader.setCustomModelResourceLocation(Item.getItemFromBlock(this), stack.getItemDamage(), new ModelResourceLocation(path, Platform.buildPropertyString(state.getProperties())));
        }
    }
}
