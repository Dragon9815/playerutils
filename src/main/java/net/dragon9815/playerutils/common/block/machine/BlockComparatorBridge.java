/*
 * This file is part of PlayerUtils
 * Copyright (C) 2016 Dragon9815 and others.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package net.dragon9815.playerutils.common.block.machine;

import net.dragon9815.playerutils.common.block.BlockTile;
import net.dragon9815.playerutils.common.capabilities.player.CapabilityPlayerHandler;
import net.dragon9815.playerutils.common.capabilities.player.IPlayerHandler;
import net.dragon9815.playerutils.common.creativetab.CreativeTabsPlayerUtils;
import net.dragon9815.playerutils.common.tileentity.machines.TileEntityComparatorBridge;
import net.dragon9815.playerutils.common.util.TileEntityHelper;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

import javax.annotation.Nullable;

public class BlockComparatorBridge extends BlockTile {
    public BlockComparatorBridge() {
        super("machines/comparatorBridge", "comparator_bridge", CreativeTabsPlayerUtils.tabMachines, TileEntityComparatorBridge.class);
    }

    @Override
    @SuppressWarnings("deprecation")
    public int getComparatorInputOverride(IBlockState blockState, World worldIn, BlockPos pos) {
        TileEntity te = TileEntityHelper.getTileEntityInWorld(worldIn, pos);

        if(te instanceof TileEntityComparatorBridge) {
            TileEntityComparatorBridge tec = (TileEntityComparatorBridge)te;

            if(tec.hasCapability(CapabilityPlayerHandler.PLAYER_HANDLER_CAPABILITY, null) && tec.canOutputToComparator()) {
                IPlayerHandler playerHandler = tec.getCapability(CapabilityPlayerHandler.PLAYER_HANDLER_CAPABILITY, null);

                EntityPlayer player = playerHandler.getPlayerEntity();

                if(player != null) {
                    return Container.calcRedstoneFromInventory(player.inventory);
                }
            }
        }

        return 0;
    }

    @Override
    @SuppressWarnings("deprecation")
    public boolean hasComparatorInputOverride(IBlockState state) {
        return true;
    }

    @Override
    public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumHand hand, @Nullable ItemStack heldItem, EnumFacing side, float hitX, float hitY, float hitZ) {
        if(!worldIn.isRemote && hand == EnumHand.MAIN_HAND)
            ;//playerIn.openGui(PlayerUtils.instance, GuiComparatorBridge.ID, worldIn, pos.getX(), pos.getY(), pos.getZ());

        return true;
    }
}
