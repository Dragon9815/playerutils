/*
 * This file is part of PlayerUtils
 * Copyright (C) 2016 Dragon9815 and others.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package net.dragon9815.playerutils.common.creativetab;


import net.dragon9815.playerutils.common.block.Blocks;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class CreativeTabsPlayerUtils {
    /*public static final CreativeTabs tabResources = new CreativeTabs("tabResources") {
        @Override
        public Item getTabIconItem() {
            return Items.DUST.getItem();
        }

        @Override
        public int getIconItemDamage() {
            return EnumOres.COPPER.getMeta();
        }
    };*/

    public static final CreativeTabs tabMachines = new CreativeTabs("tabMachines") {
        @Override
        public Item getTabIconItem() {
            return Item.getItemFromBlock(Blocks.ITEM_TRANSPORTER.getBlock());
        }
    };

}
