/*
 * This file is part of PlayerUtils
 * Copyright (C) 2016 Dragon9815 and others.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package net.dragon9815.playerutils.common.integration.waila;

import mcp.mobius.waila.api.IWailaDataProvider;
import mcp.mobius.waila.api.IWailaRegistrar;
import net.dragon9815.playerutils.common.integration.ModIntegrationBase;
import net.dragon9815.playerutils.common.reference.IntegrationModIDs;
import net.dragon9815.playerutils.common.tileentity.TileEntityBase;
import net.minecraftforge.fml.common.event.FMLInterModComms;

public class ModIntegrationWaila extends ModIntegrationBase {

    @SuppressWarnings("unused")
    public static void register(IWailaRegistrar registrar) {
        IWailaDataProvider provider = new TileWailaDataProvider();

        registrar.registerHeadProvider(provider, TileEntityBase.class);
        registrar.registerBodyProvider(provider, TileEntityBase.class);
    }

    public ModIntegrationWaila() {
        super(IntegrationModIDs.WAILA);
    }

    public void init() throws Exception {
        FMLInterModComms.sendMessage(getModId(), "register", ModIntegrationWaila.class.getName() + ".register");
    }
}
