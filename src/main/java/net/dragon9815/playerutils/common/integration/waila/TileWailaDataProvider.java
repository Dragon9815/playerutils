/*
 * This file is part of PlayerUtils
 * Copyright (C) 2016 Dragon9815 and others.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package net.dragon9815.playerutils.common.integration.waila;

import mcp.mobius.waila.api.IWailaConfigHandler;
import mcp.mobius.waila.api.IWailaDataAccessor;
import mcp.mobius.waila.api.IWailaDataProvider;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

import java.util.ArrayList;
import java.util.List;

public class TileWailaDataProvider implements IWailaDataProvider {
    private final List<IWailaDataProvider> providers = new ArrayList<IWailaDataProvider>();

    public TileWailaDataProvider() {
        providers.add(new EnergyTileProvider());
        providers.add(new PlayerTileDataProvider());
    }

    @Override
    public ItemStack getWailaStack(IWailaDataAccessor iWailaDataAccessor, IWailaConfigHandler iWailaConfigHandler) {
        return null;
    }

    @Override
    public List<String> getWailaHead(ItemStack itemStack, List<String> currentTooltip, IWailaDataAccessor iWailaDataAccessor, IWailaConfigHandler iWailaConfigHandler) {
        for (IWailaDataProvider p : providers) {
            p.getWailaHead(itemStack, currentTooltip, iWailaDataAccessor, iWailaConfigHandler);
        }

        return currentTooltip;
    }

    @Override
    public List<String> getWailaBody(ItemStack itemStack, List<String> currentTooltip, IWailaDataAccessor iWailaDataAccessor, IWailaConfigHandler iWailaConfigHandler) {
        for (IWailaDataProvider p : providers) {
            p.getWailaBody(itemStack, currentTooltip, iWailaDataAccessor, iWailaConfigHandler);
        }

        return currentTooltip;
    }

    @Override
    public List<String> getWailaTail(ItemStack itemStack, List<String> currentTooltip, IWailaDataAccessor iWailaDataAccessor, IWailaConfigHandler iWailaConfigHandler) {
        for (IWailaDataProvider p : providers) {
            p.getWailaTail(itemStack, currentTooltip, iWailaDataAccessor, iWailaConfigHandler);
        }

        return currentTooltip;
    }

    @Override
    public NBTTagCompound getNBTData(EntityPlayerMP entityPlayerMP, TileEntity tileEntity, NBTTagCompound nbtTagCompound, World world, BlockPos blockPos) {
        for (IWailaDataProvider p : providers) {
            p.getNBTData(entityPlayerMP, tileEntity, nbtTagCompound, world, blockPos);
        }

        return nbtTagCompound;
    }
}
