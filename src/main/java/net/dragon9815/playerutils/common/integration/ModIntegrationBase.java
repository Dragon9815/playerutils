package net.dragon9815.playerutils.common.integration;

import net.dragon9815.playerutils.common.helper.LogHelper;
import net.minecraftforge.fml.common.Loader;

public abstract class ModIntegrationBase {

    private final String modId;
    private boolean loaded;

    protected ModIntegrationBase(String modId) {
        this.modId = modId;
    }

    public final String getModId() {
        return modId;
    }

    public final void tryLoad() throws Exception {
        if(!Loader.isModLoaded(modId)) {
            LogHelper.info(String.format("Integration for \'%s\' could not be loaded: Mod not present", modId));
            loaded = false;
        }

        init();

        LogHelper.info(String.format("Integration for \'%s\' loaded", modId));
        loaded = true;
    }

    public final boolean isLoaded() {
        return loaded;
    }

    protected abstract void init() throws Exception;
}
