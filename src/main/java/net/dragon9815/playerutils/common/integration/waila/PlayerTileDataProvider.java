package net.dragon9815.playerutils.common.integration.waila;

import mcp.mobius.waila.api.IWailaConfigHandler;
import mcp.mobius.waila.api.IWailaDataAccessor;
import mcp.mobius.waila.api.IWailaDataProvider;
import net.dragon9815.playerutils.common.capabilities.player.CapabilityPlayerHandler;
import net.dragon9815.playerutils.common.capabilities.player.IPlayerHandler;
import net.dragon9815.playerutils.common.tileentity.machines.TileEntityItemTransporter;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

import java.util.List;

public class PlayerTileDataProvider implements IWailaDataProvider {
    @Override
    public ItemStack getWailaStack(IWailaDataAccessor accessor, IWailaConfigHandler config) {
        return null;
    }

    @Override
    public List<String> getWailaHead(ItemStack itemStack, List<String> currenttip, IWailaDataAccessor accessor, IWailaConfigHandler config) {
        return currenttip;
    }

    @Override
    public List<String> getWailaBody(ItemStack itemStack, List<String> currentTooltip, IWailaDataAccessor iWailaDataAccessor, IWailaConfigHandler iWailaConfigHandler) {
        TileEntity te = iWailaDataAccessor.getTileEntity();

        if(te.hasCapability(CapabilityPlayerHandler.PLAYER_HANDLER_CAPABILITY, null)) {
            IPlayerHandler playerHandler = te.getCapability(CapabilityPlayerHandler.PLAYER_HANDLER_CAPABILITY, null);

            if(playerHandler.getPlayer() != null && playerHandler.getPlayer().isValid()) {
                currentTooltip.add(String.format("Bound to: %s", playerHandler.getPlayer().name));
            }
            else {
                currentTooltip.add("No Player Bound! Insert Player Card");
            }
        }

        return currentTooltip;
    }

    @Override
    public List<String> getWailaTail(ItemStack itemStack, List<String> currenttip, IWailaDataAccessor accessor, IWailaConfigHandler config) {
        return currenttip;
    }

    @Override
    public NBTTagCompound getNBTData(EntityPlayerMP player, TileEntity te, NBTTagCompound tag, World world, BlockPos pos) {
        return tag;
    }
}
