package net.dragon9815.playerutils.common.integration;

import net.dragon9815.playerutils.common.helper.LogHelper;
import net.dragon9815.playerutils.common.integration.waila.ModIntegrationWaila;

public class IntegrationManager {
    // Integrations
    public static final ModIntegrationWaila WAILA = new ModIntegrationWaila();

    private static ModIntegrationBase[] integrations = new ModIntegrationBase[] { WAILA };

    public static void loadAll() {
        for(ModIntegrationBase base : integrations) {
            try {
                base.tryLoad();
            }
            catch (Exception e) {
                LogHelper.info(String.format("Integration for \'%s\' could not be loaded: Exceptions raised during loading!", base.getModId()));
                e.printStackTrace();
            }
        }
    }
}
