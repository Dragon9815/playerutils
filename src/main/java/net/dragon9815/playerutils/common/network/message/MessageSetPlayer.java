/*
 * This file is part of PlayerUtils
 * Copyright (C) 2016 Dragon9815 and others.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package net.dragon9815.playerutils.common.network.message;

import io.netty.buffer.ByteBuf;
import net.dragon9815.playerutils.common.capabilities.player.CapabilityPlayerHandler;
import net.dragon9815.playerutils.common.capabilities.player.IPlayerHandler;
import net.dragon9815.playerutils.common.util.PlayerData;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.common.DimensionManager;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class MessageSetPlayer implements IMessage, IMessageHandler<MessageSetPlayer, MessageSetPlayer> {
    PlayerData playerData;
    BlockPos tilePos;
    int dimID;

    public MessageSetPlayer() {
        this(null, null);
    }

    public MessageSetPlayer(PlayerData playerData, TileEntity te) {
        this.playerData = playerData;
        if(te != null) {
            this.tilePos = te.getPos();
            this.dimID = te.getWorld().provider.getDimension();
        }
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        NBTTagCompound tagCompound = ByteBufUtils.readTag(buf);

        NBTTagCompound playerTag = tagCompound.getCompoundTag("player");

        this.playerData = PlayerData.readFromToNBT(playerTag);

        int[] coords = tagCompound.getIntArray("pos");
        dimID = coords[0];
        tilePos = new BlockPos(coords[1], coords[2], coords[3]);
    }

    @Override
    public void toBytes(ByteBuf buf) {
        NBTTagCompound tagCompound = new NBTTagCompound();

        NBTTagCompound playerTag = new NBTTagCompound();
        playerData.writeToNBT(playerTag);
        tagCompound.setTag("player", playerTag);

        tagCompound.setIntArray("pos", new int[] {dimID, tilePos.getX(), tilePos.getY(), tilePos.getZ()});

        ByteBufUtils.writeTag(buf, tagCompound);
    }

    @Override
    public MessageSetPlayer onMessage(MessageSetPlayer message, MessageContext ctx) {

        if(message.playerData == null || message.tilePos == null)
            return null;

        World world = DimensionManager.getWorld(message.dimID);

        if(world == null)
            return null;

        TileEntity te = world.getTileEntity(message.tilePos);

        if(te == null)
            return null;

        if(te.hasCapability(CapabilityPlayerHandler.PLAYER_HANDLER_CAPABILITY, null)) {
            IPlayerHandler handler = te.getCapability(CapabilityPlayerHandler.PLAYER_HANDLER_CAPABILITY, null);

            handler.setPlayer(message.playerData);
        }

        return null;
    }
}
