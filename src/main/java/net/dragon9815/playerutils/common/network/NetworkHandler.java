/*
 * This file is part of PlayerUtils
 * Copyright (C) 2016 Dragon9815 and others.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package net.dragon9815.playerutils.common.network;

import net.dragon9815.playerutils.common.network.message.MessagePlayerListUpdate;
import net.dragon9815.playerutils.common.network.message.MessagePrintInfo;
import net.dragon9815.playerutils.common.network.message.MessageSetPlayer;
import net.dragon9815.playerutils.common.reference.Reference;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import net.minecraftforge.fml.relauncher.Side;

public class NetworkHandler {
    public static SimpleNetworkWrapper INSTANCE = NetworkRegistry.INSTANCE.newSimpleChannel(Reference.MOD_ID);

    public static void init() {
        INSTANCE.registerMessage(MessagePrintInfo.class, MessagePrintInfo.class, 0, Side.SERVER);
        INSTANCE.registerMessage(MessagePlayerListUpdate.class, MessagePlayerListUpdate.class, 1, Side.CLIENT);
        INSTANCE.registerMessage(MessageSetPlayer.class, MessageSetPlayer.class, 2, Side.SERVER);
    }
}
