/*
 * This file is part of PlayerUtils
 * Copyright (C) 2016 Dragon9815 and others.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package net.dragon9815.playerutils.common.network.message;

import io.netty.buffer.ByteBuf;
import net.minecraft.block.Block;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.inventory.IInventory;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.world.WorldServer;
import net.minecraftforge.common.DimensionManager;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

import java.util.UUID;

public class MessagePrintInfo implements IMessage, IMessageHandler<MessagePrintInfo, MessagePrintInfo> {

    private int x;
    private int y;
    private int z;
    private int dimID;
    private UUID playerUUID;

    public MessagePrintInfo() {
    }

    public MessagePrintInfo(int x, int y, int z, int dimID, UUID playerUUID) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.dimID = dimID;
        this.playerUUID = playerUUID;
    }


    @Override
    public void fromBytes(ByteBuf buf) {
        NBTTagCompound tagCompound = ByteBufUtils.readTag(buf);

        this.x = tagCompound.getInteger("x");
        this.y = tagCompound.getInteger("y");
        this.z = tagCompound.getInteger("z");
        this.dimID = tagCompound.getInteger("dimID");
        this.playerUUID = UUID.fromString(tagCompound.getString("playerUUID"));
    }

    @Override
    public void toBytes(ByteBuf buf) {
        NBTTagCompound tagCompound = new NBTTagCompound();

        tagCompound.setInteger("x", x);
        tagCompound.setInteger("y", y);
        tagCompound.setInteger("z", z);
        tagCompound.setInteger("dimID", dimID);
        tagCompound.setString("playerUUID", playerUUID.toString());

        ByteBufUtils.writeTag(buf, tagCompound);
    }

    @Override
    public MessagePrintInfo onMessage(MessagePrintInfo message, MessageContext ctx) {
        WorldServer world = DimensionManager.getWorld(message.dimID);

        if (world == null)
            return null;

        EntityPlayerMP player = world.getMinecraftServer().getPlayerList().getPlayerByUUID(message.playerUUID);

        if (player == null)
            return null;

        Block block = world.getBlockState(new BlockPos(message.x, message.y, message.z)).getBlock();

        if (block == null)
            return null;

        player.addChatMessage(new TextComponentString(block.getLocalizedName() + ":"));

        TileEntity te = world.getTileEntity(new BlockPos(message.x, message.y, message.z));

        if (te == null)
            return null;

        if (te instanceof IInventory) {
            IInventory inv = (IInventory) te;

            for (int i = 0; i < inv.getSizeInventory(); i++) {
                player.addChatMessage(new TextComponentString(String.valueOf(inv.getStackInSlot(i))));
            }
        }


        return null;
    }
}
