/*
 * This file is part of PlayerUtils
 * Copyright (C) 2016 Dragon9815 and others.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package net.dragon9815.playerutils.common.network.message;

import io.netty.buffer.ByteBuf;
import net.dragon9815.playerutils.common.helper.PlayerHelper;
import net.dragon9815.playerutils.common.util.PlayerData;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

import java.util.ArrayList;

public class MessagePlayerListUpdate implements IMessage, IMessageHandler<MessagePlayerListUpdate, IMessage> {
    private ArrayList<PlayerData> players;

    public MessagePlayerListUpdate(ArrayList<PlayerData> players) {
        this.players = players;
    }

    public MessagePlayerListUpdate() {
        this.players = new ArrayList<PlayerData>();
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        NBTTagCompound tag = ByteBufUtils.readTag(buf);

        NBTTagList tagList = tag.getTagList("players", 10);
        for (int i = 0; i < tagList.tagCount(); i++) {
            NBTTagCompound tagCompound = tagList.getCompoundTagAt(i);
            players.add(PlayerData.readFromToNBT(tagCompound));
        }
    }

    @Override
    public void toBytes(ByteBuf buf) {
        NBTTagCompound tag = new NBTTagCompound();

        NBTTagList tagList = new NBTTagList();
        for(PlayerData data : players) {
            NBTTagCompound tagCompound = new NBTTagCompound();
            data.writeToNBT(tagCompound);
            tagList.appendTag(tagCompound);
        }

        tag.setTag("players", tagList);

        ByteBufUtils.writeTag(buf, tag);
    }

    @Override
    public IMessage onMessage(MessagePlayerListUpdate message, MessageContext ctx) {

        PlayerHelper.INSTANCE.load(message.players);

        return null;
    }
}
