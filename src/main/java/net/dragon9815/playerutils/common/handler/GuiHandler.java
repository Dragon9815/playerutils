/*
 * This file is part of PlayerUtils
 * Copyright (C) 2016 Dragon9815 and others.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package net.dragon9815.playerutils.common.handler;

import net.dragon9815.playerutils.client.gui.GuiFeeder;
import net.dragon9815.playerutils.client.gui.GuiItemTransporter;
import net.dragon9815.playerutils.common.inventory.container.ContainerFeeder;
import net.dragon9815.playerutils.common.inventory.container.ContainerItemTransporter;
import net.dragon9815.playerutils.common.tileentity.machines.TileEntityFeeder;
import net.dragon9815.playerutils.common.tileentity.machines.TileEntityItemTransporter;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.network.IGuiHandler;

public class
GuiHandler implements IGuiHandler {
    @Override
    public Object getServerGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
        TileEntity te = world.getTileEntity(new BlockPos(x, y, z));

        switch(ID) {
            case GuiItemTransporter.ID:
                if(te instanceof TileEntityItemTransporter)
                    return new ContainerItemTransporter(player.inventory, (TileEntityItemTransporter)te);
                break;

            case GuiFeeder.ID:
                if(te instanceof TileEntityFeeder)
                    return new ContainerFeeder(player.inventory, (TileEntityFeeder)te);
                break;
        }

        return null;
    }

    @Override
    public Object getClientGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
        TileEntity te = world.getTileEntity(new BlockPos(x, y, z));

        switch(ID) {
            case GuiItemTransporter.ID:
                if(te instanceof TileEntityItemTransporter)
                    return new GuiItemTransporter((TileEntityItemTransporter)te, player.inventory);
                break;

            case GuiFeeder.ID:
                if(te instanceof TileEntityFeeder)
                    return new GuiFeeder((TileEntityFeeder)te, player.inventory);
                break;
        }

        return null;
    }
}
