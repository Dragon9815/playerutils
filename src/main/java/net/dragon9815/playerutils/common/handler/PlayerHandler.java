/*
 * This file is part of PlayerUtils
 * Copyright (C) 2016 Dragon9815 and others.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package net.dragon9815.playerutils.common.handler;

import net.dragon9815.playerutils.common.helper.PlayerHelper;
import net.dragon9815.playerutils.common.network.NetworkHandler;
import net.dragon9815.playerutils.common.network.message.MessagePlayerListUpdate;
import net.dragon9815.playerutils.common.util.PlayerData;
import net.dragon9815.playerutils.common.world.WorldDataPlayerUtils;
import net.minecraftforge.fml.common.eventhandler.EventPriority;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent;

public class PlayerHandler {

    @SubscribeEvent(priority = EventPriority.HIGH)
    public void playerLogin(PlayerEvent.PlayerLoggedInEvent event) {
        PlayerHelper.INSTANCE.setPlayerOnline(PlayerData.fromPlayer(event.player));
        WorldDataPlayerUtils.getData(event.player.worldObj);
        NetworkHandler.INSTANCE.sendToAll(new MessagePlayerListUpdate(PlayerHelper.INSTANCE.getAllPlayers()));
    }

    @SubscribeEvent(priority = EventPriority.HIGH)
    public void onPlayerLeave(PlayerEvent.PlayerLoggedOutEvent event) {
        PlayerHelper.INSTANCE.setPlayerOffline(PlayerData.fromPlayer(event.player));
        WorldDataPlayerUtils.getData(event.player.worldObj);
        NetworkHandler.INSTANCE.sendToAll(new MessagePlayerListUpdate(PlayerHelper.INSTANCE.getAllPlayers()));
    }
}
