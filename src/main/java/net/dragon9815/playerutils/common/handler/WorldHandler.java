/*
 * This file is part of PlayerUtils
 * Copyright (C) 2016 Dragon9815 and others.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package net.dragon9815.playerutils.common.handler;

import net.dragon9815.playerutils.common.helper.PlayerHelper;
import net.dragon9815.playerutils.common.reference.Reference;
import net.dragon9815.playerutils.common.util.PlayerData;
import net.dragon9815.playerutils.common.world.WorldDataPlayerUtils;
import net.minecraft.world.WorldSavedData;
import net.minecraftforge.event.world.WorldEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

import java.util.ArrayList;

public class WorldHandler {

    @SubscribeEvent
    public void onWorldSave(WorldEvent.Save event) {
        if(!event.getWorld().isRemote) {
            WorldDataPlayerUtils data = new WorldDataPlayerUtils(Reference.MOD_ID + ".players");

            data.setPlayers(PlayerHelper.INSTANCE.getAllPlayers());

            event.getWorld().setItemData(Reference.MOD_ID + ".players", data);
        }
    }

    @SubscribeEvent
    public void onWorldLoad(WorldEvent.Load event) {
        if(!event.getWorld().isRemote) {
            PlayerHelper.INSTANCE.clearCache();
            PlayerHelper.INSTANCE.load(new ArrayList<PlayerData>());
            WorldSavedData data = event.getWorld().loadItemData(WorldDataPlayerUtils.class, Reference.MOD_ID + ".players");

            if(data instanceof WorldDataPlayerUtils) {
                PlayerHelper.INSTANCE.load(((WorldDataPlayerUtils)data).players);
            }
        }
    }
}
