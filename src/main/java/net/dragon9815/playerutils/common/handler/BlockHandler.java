/*
 * This file is part of PlayerUtils
 * Copyright (C) 2016 Dragon9815 and others.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package net.dragon9815.playerutils.common.handler;

import net.minecraftforge.event.world.BlockEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class BlockHandler {

    @SubscribeEvent
    @SuppressWarnings("unused")
    public void onBlockBreak(BlockEvent.BreakEvent event) {
        /*TileEntity te = event.getWorld().getTileEntity(event.getPos());

        if(te != null) {
            if (te.hasCapability(CapabilityEnergyHandler.ENERGY_HANDLER_CAPABILITY, null)) {
                IEnergyHandler energyHandler = te.getCapability(CapabilityEnergyHandler.ENERGY_HANDLER_CAPABILITY, null);
                DimensionalBlockPos masterPos = energyHandler.getMasterPos();

                if (masterPos != null) {
                    TileEntity te2 = TileEntityHelper.getTileEntityInDimension(masterPos);

                    if (te2 instanceof TileEntityEnergyHub) {
                        TileEntityEnergyHub energyHub = ((TileEntityEnergyHub) te2);
                        energyHub.removeConnection(new DimensionalBlockPos(event.getWorld().provider.getDimension(), event.getPos()));
                        energyHandler.setMasterPos(null);
                    }
                }
            } else if (te instanceof TileEntityEnergyHub) {
                TileEntityEnergyHub energyHub = (TileEntityEnergyHub) te;

                for (DimensionalBlockPos pos : energyHub.getConnections()) {
                    TileEntity te2 = TileEntityHelper.getTileEntityInDimension(pos);

                    if (te2 != null && te2.hasCapability(CapabilityEnergyHandler.ENERGY_HANDLER_CAPABILITY, null)) {
                        IEnergyHandler energyHandler = te2.getCapability(CapabilityEnergyHandler.ENERGY_HANDLER_CAPABILITY, null);
                        energyHandler.setMasterPos(null);
                    }
                }
            }
        }*/
    }
}
