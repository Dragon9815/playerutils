/*
 * This file is part of PlayerUtils
 * Copyright (C) 2016 Dragon9815 and others.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package net.dragon9815.playerutils.proxy;

import net.dragon9815.playerutils.PlayerUtils;
import net.dragon9815.playerutils.common.block.Blocks;
import net.dragon9815.playerutils.common.capabilities.player.CapabilityPlayerHandler;
import net.dragon9815.playerutils.common.capabilities.upgrade.CapabilityUpgradeHandler;
import net.dragon9815.playerutils.common.handler.GuiHandler;
import net.dragon9815.playerutils.common.handler.PlayerHandler;
import net.dragon9815.playerutils.common.handler.WorldHandler;
import net.dragon9815.playerutils.common.integration.IntegrationManager;
import net.dragon9815.playerutils.common.item.Items;
import net.dragon9815.playerutils.common.network.NetworkHandler;
import net.dragon9815.playerutils.common.world.WorldGen;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.common.registry.GameRegistry;

public abstract class CommonProxy implements IProxy {
    @Override
    public void registerOreDict() {

    }

    @Override
    public void registerRecipes() {
        Blocks.registerRecipes();
        Items.registerRecipes();
    }

    @Override
    public void registerWorldGen() {
        WorldGen worldGen = new WorldGen();
        GameRegistry.registerWorldGenerator(worldGen, 0);
        MinecraftForge.EVENT_BUS.register(worldGen);
    }

    @Override
    public void registerHandlers() {
        MinecraftForge.EVENT_BUS.register(new PlayerHandler());
        MinecraftForge.EVENT_BUS.register(new WorldHandler());
    }

    @Override
    public void registerCapabilities() {
        CapabilityUpgradeHandler.enable();
        CapabilityPlayerHandler.enable();
    }

    @Override
    public void registerRenderers() {}

    @Override
    public void preInit() {
        registerCapabilities();

        Items.registerAll();
        Blocks.registerAll();

        registerRenderers();
    }

    @Override
    public void init() {
        registerRecipes();
        registerWorldGen();
        registerHandlers();
    }

    @Override
    public void postInit() {
        NetworkHandler.init();
        NetworkRegistry.INSTANCE.registerGuiHandler(PlayerUtils.instance, new GuiHandler());

        IntegrationManager.loadAll();
    }
}
