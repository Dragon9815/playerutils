/*
 * This file is part of PlayerUtils
 * Copyright (C) 2016 Dragon9815 and others.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package net.dragon9815.playerutils.proxy;

import net.dragon9815.playerutils.client.model.item.ModelPlayerCard;
import net.dragon9815.playerutils.common.block.Blocks;
import net.dragon9815.playerutils.common.handler.BlockHandler;
import net.dragon9815.playerutils.common.handler.RenderHandler;
import net.dragon9815.playerutils.common.item.Items;
import net.minecraftforge.client.model.ModelLoaderRegistry;
import net.minecraftforge.common.MinecraftForge;

public class ClientProxy extends CommonProxy {

    @Override
    public void registerHandlers() {
        super.registerHandlers();
        MinecraftForge.EVENT_BUS.register(new RenderHandler());
        MinecraftForge.EVENT_BUS.register(new BlockHandler());
    }

    @Override
    public void preInit() {
        super.preInit();
    }

    @Override
    public void registerRenderers() {
        ModelLoaderRegistry.registerLoader(new ModelPlayerCard.PlayerCardModelLoader());

        Blocks.registerRenderers();
        Items.registerRenderers();
    }
}
